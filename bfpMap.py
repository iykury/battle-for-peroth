import random, math, os
from PIL import Image

#my own sign function, because fuck it math.copysign is for horrible people
def sign(x):
	if x > 0:
		return 1
	elif x < 0:
		return -1
	else:
		return 0
def signPos(x):
	if x >= 0:
		return 1
	elif x < 0:
		return -1
def signNeg(x):
	if x > 0:
		return 1
	elif x <= 0:
		return -1

def generate(boardSize):

	#Do folder stuff
	file_path = os.path.abspath(__file__)
	folder = os.path.dirname(file_path)

	Width = max(400, 16 + 32 * boardSize)
	Height = max(300, 12 + 24 * boardSize)

	#Phase 1: Generate a map
	arrayGeo = [["grass" for x in range(0,boardSize)] for y in range(0,boardSize)]	#Right now, it just fills the entire board with grass

	#Generate river
	rivPotential = boardSize ** 2 / 144
	rivEnds = []
	while rivPotential > 0:
		rivStart = [0, 0, "X"] #X Coord, Y Coord, direction
		rivEnd = [0, 0, "X"] #X Coord, Y Coord, direction
		if random.random() >= (1/2) ** rivPotential:	#Will this map have a river?
			yeetRivStartDir = random.random()
			rivMode = 1
			sharpUTurnPreventer = 0

			if yeetRivStartDir < 1/4:	#Starts from left
				rivx = 0
				rivy = random.randint(0,boardSize-1)
				rivStart[0] = -16 * min(boardSize, 12) + (rivx * 32)
				rivStart[1] = Height//2 - 12 * boardSize + (rivy * 24)
				rivStart[2] = "l"
				rivEnd[2] = "r"

				while rivx < boardSize and rivy in range(0,boardSize):
					yeetRiv = random.random()
					if rivMode == 1:
						if yeetRiv < 1/8 and sharpUTurnPreventer >= 0:	#Curves up
							arrayGeo[rivx][rivy] = "river_UL"
							rivMode = 0
							rivEnd[2] = "u"
							rivy -= 1
						elif yeetRiv >= 7/8 and sharpUTurnPreventer <= 0:	#Curves down
							arrayGeo[rivx][rivy] = "river_DL"
							rivMode = 2
							rivEnd[2] = "d"
							rivy += 1
						else:	#Goes straight
							arrayGeo[rivx][rivy] = "river_LR"
							rivx += 1
							sharpUTurnPreventer = 0
					elif rivMode == 0:	#If going up
						if yeetRiv < 1/2:	#Goes straight
							arrayGeo[rivx][rivy] = "river_UD"
							rivy -= 1
						else:	#Curves back right
							arrayGeo[rivx][rivy] = "river_DR"
							rivMode = 1
							rivEnd[2] = "r"
							rivx += 1
							sharpUTurnPreventer = 1
					elif rivMode == 2:	#If going down
						if yeetRiv < 1/2:	#Goes straight
							arrayGeo[rivx][rivy] = "river_UD"
							rivy += 1
						else:	#Curves back right
							arrayGeo[rivx][rivy] = "river_UR"
							rivMode = 1
							rivEnd[2] = "r"
							rivx += 1
							sharpUTurnPreventer = -1

			elif yeetRivStartDir < 1/2:	#Starts from top
				rivx = random.randint(0,boardSize-1)#/2) + random.randint(0,boardSize/2) 
				rivy = 0
				rivStart[0] = Width//2 - 16 * boardSize + (rivx * 32)
				rivStart[1] = -12 * min(boardSize, 12) + (rivy * 24)
				rivStart[2] = "u"
				rivEnd[2] = "d"

				while rivy < boardSize and rivx in range(0,boardSize):
					yeetRiv = random.random()
					if rivMode == 1:
						if yeetRiv < 1/8 and sharpUTurnPreventer >= 0:	#Curves left
							arrayGeo[rivx][rivy] = "river_UL"
							rivMode = 0
							rivEnd[2] = "l"
							rivx -= 1
						elif yeetRiv >= 7/8 and sharpUTurnPreventer <= 0:	#Curves right
							arrayGeo[rivx][rivy] = "river_UR"
							rivMode = 2
							rivEnd[2] = "r"
							rivx += 1
						else:	#Goes straight
							arrayGeo[rivx][rivy] = "river_UD"
							rivy += 1
							sharpUTurnPreventer = 0
					elif rivMode == 0:	#If going left
						if yeetRiv < 1/2:	#Goes straight
							arrayGeo[rivx][rivy] = "river_LR"
							rivx -= 1
						else:	#Curves back down
							arrayGeo[rivx][rivy] = "river_DR"
							rivMode = 1
							rivEnd[2] = "d"
							rivy += 1
							sharpUTurnPreventer = 1
					elif rivMode == 2:	#If going right
						if yeetRiv < 1/2:	#Goes straight
							arrayGeo[rivx][rivy] = "river_LR"
							rivx += 1
						else:	#Curves back down
							arrayGeo[rivx][rivy] = "river_DL"
							rivMode = 1
							rivEnd[2] = "d"
							rivy += 1
							sharpUTurnPreventer = -1

			elif yeetRivStartDir < 3/4:	#Starts from right
				rivx = boardSize - 1
				rivy = random.randint(0,boardSize-1)#/2) + random.randint(0,boardSize/2)
				rivStart[0] = (Width//2 + 32) - 16 * boardSize + (rivx * 32)
				rivStart[1] = Height//2 - 12 * boardSize + (rivy * 24)
				rivStart[2] = "r"
				rivEnd[2] = "l"

				while rivx >= 0 and rivy in range(0,boardSize):
					yeetRiv = random.random()
					if rivMode == 1:
						if yeetRiv < 1/8 and sharpUTurnPreventer >= 0:	#Curves up
							arrayGeo[rivx][rivy] = "river_UR"
							rivMode = 0
							rivEnd[2] = "u"
							rivy -= 1
						elif yeetRiv >= 7/8 and sharpUTurnPreventer <= 0:	#Curves down
							arrayGeo[rivx][rivy] = "river_DR"
							rivMode = 2
							rivEnd[2] = "d"
							rivy += 1
						else:	#Goes straight
							arrayGeo[rivx][rivy] = "river_LR"
							rivx -= 1
							sharpUTurnPreventer = 0
					elif rivMode == 0:	#If going up
						if yeetRiv < 1/2:	#Goes straight
							arrayGeo[rivx][rivy] = "river_UD"
							rivy -= 1
						else:	#Curves back left
							arrayGeo[rivx][rivy] = "river_DL"
							rivMode = 1
							rivEnd[2] = "l"
							rivx -= 1
							sharpUTurnPreventer = 1
					elif rivMode == 2:	#If going down
						if yeetRiv < 1/2:	#Goes straight
							arrayGeo[rivx][rivy] = "river_UD"
							rivy += 1
						else:	#Curves back left
							arrayGeo[rivx][rivy] = "river_UL"
							rivMode = 1
							rivEnd[2] = "l"
							rivx -= 1
							sharpUTurnPreventer = -1

			else:	#Starts from bottom
				rivx = random.randint(0,boardSize-1)#/2) + random.randint(0,boardSize/2) 
				rivy = boardSize - 1
				rivStart[0] = Width//2 - 16 * boardSize + (rivx * 32)
				rivStart[1] = (Height//2 + 24) - 12 * boardSize + (rivy * 24)
				rivStart[2] = "d"
				rivEnd[2] = "u"

				while rivy >= 0 and rivx in range(0,boardSize):
					yeetRiv = random.random()
					if rivMode == 1:
						if yeetRiv < 1/8 and sharpUTurnPreventer >= 0:	#Curves left
							arrayGeo[rivx][rivy] = "river_DL"
							rivMode = 0
							rivEnd[2] = "l"
							rivx -= 1
						elif yeetRiv >= 7/8 and sharpUTurnPreventer <= 0:	#Curves right
							arrayGeo[rivx][rivy] = "river_DR"
							rivMode = 2
							rivEnd[2] = "r"
							rivx += 1
						else:	#Goes straight
							arrayGeo[rivx][rivy] = "river_UD"
							rivy -= 1
							sharpUTurnPreventer = 0
					elif rivMode == 0:	#If going left
						if yeetRiv < 1/2:	#Goes straight
							arrayGeo[rivx][rivy] = "river_LR"
							rivx -= 1
						else:	#Curves back up
							arrayGeo[rivx][rivy] = "river_UR"
							rivMode = 1
							rivEnd[2] = "u"
							rivy -= 1
							sharpUTurnPreventer = 1
					elif rivMode == 2:	#If going right
						if yeetRiv < 1/2:	#Goes straight
							arrayGeo[rivx][rivy] = "river_LR"
							rivx += 1
						else:	#Curves back up
							arrayGeo[rivx][rivy] = "river_UL"
							rivMode = 1
							rivEnd[2] = "u"
							rivy -= 1
							sharpUTurnPreventer = -1

			if rivEnd[2] == "l":
				rivEnd[0] = 32 - 16 * min(boardSize, 12) + (rivx * 32)
				rivEnd[1] = Height//2 - 12 * boardSize + (rivy * 24)
			elif rivEnd[2] == "u":
				rivEnd[0] = Width//2 - 16 * boardSize + (rivx * 32)
				rivEnd[1] = 24 - 12 * min(boardSize, 12) + (rivy * 24)
			elif rivEnd[2] == "r":
				rivEnd[0] = Width//2 - 16 * boardSize + (rivx * 32)
				rivEnd[1] = Height//2 - 12 * boardSize + (rivy * 24)
			elif rivEnd[2] == "d":
				rivEnd[0] = Width//2 - 16 * boardSize + (rivx * 32)
				rivEnd[1] = Height//2 - 12 * boardSize + (rivy * 24)

		rivPotential -= math.sqrt(boardSize ** 2 / 144)

	#Generate bridges
	if rivStart[2] != "X":
		#Step 1: Find out how many bridges we will need, and where they can be placed
		arrRivTiles = ["river_DL","river_DR","river_LR","river_UD","river_UL","river_UR"]
		arrRivStraights = ["river_LR","river_UD"]
		noRivTiles = 0
		arrRiver = []	#Contains all the tiles a bridge can be put over
		for wai in range(0, boardSize):
			for eks in range(0, boardSize):
				if arrayGeo[eks][wai] in arrRivTiles:
					noRivTiles += 1
					if arrayGeo[eks][wai] in arrRivStraights and not(arrayGeo[eks][wai] == "river_LR" and wai in [0, boardSize - 1]) and not(arrayGeo[eks][wai] == "river_UD" and eks in [0, boardSize - 1]):
						arrRiver.append((eks,wai))

		#Step 2: Place bridges
		noBridges = 0
		while noBridges < noRivTiles / 8 and len(arrRiver) > 0:
			yeetBridge = random.randint(0, len(arrRiver) - 1)
			buildX = arrRiver[yeetBridge][0]
			buildY = arrRiver[yeetBridge][1]
			if arrayGeo[buildX][buildY] == "river_LR":
				arrayGeo[buildX][buildY] = "river_LR_bridge"
				noBridges += 1
				arrRiver.pop(yeetBridge)
			elif arrayGeo[buildX][buildY] == "river_UD":
				arrayGeo[buildX][buildY] = "river_UD_bridge"
				noBridges += 1
				arrRiver.pop(yeetBridge)
			#Check for any adjacent river tiles
			arrAdjacents = []
			if buildX != 0:
				arrAdjacents.append((buildX - 1, buildY))
			if buildX != boardSize - 1:
				arrAdjacents.append((buildX + 1, buildY))
			if buildY != 0:
				arrAdjacents.append((buildX, buildY - 1))
			if buildY != boardSize - 1:
				arrAdjacents.append((buildX, buildY + 1))
			for i in arrAdjacents:
				if i in arrRiver:
					arrRiver.remove(i)


	#Phase 2: Calculate the collision
	arrCol = [["clear" for x in range(0,boardSize)] for y in range(0,boardSize)]
	for wai in range(0, boardSize):
		for eks in range(0, boardSize):
			arrCorrespClear = ["grass", "river_LR_bridge", "river_UD_bridge"]
			arrCorrespGap = ["river_DL", "river_DR", "river_LR", "river_UD", "river_UL", "river_UR"]
			if arrayGeo[eks][wai] in arrCorrespClear:
				arrCol[eks][wai] = "clear"
			elif arrayGeo[eks][wai] in arrCorrespGap:
				arrCol[eks][wai] = "gap"

	lineCol = [["clear" for x in range(0,boardSize)] for y in range(0,boardSize)]
	for wai in range(0, boardSize):
		for eks in range(0, boardSize):
			arrCorrespClear = ["grass", "river_LR_bridge", "river_UD_bridge", "river_DL", "river_DR", "river_LR", "river_UD", "river_UL", "river_UR"]
			arrCorrespCircle = []
			arrCorrespSquare = []
			if arrayGeo[eks][wai] in arrCorrespClear:
				lineCol[eks][wai] = "clear"
			elif arrayGeo[eks][wai] in arrCorrespCircle:
				lineCol[eks][wai] = "circle"
			elif arrayGeo[eks][wai] in arrCorrespSquare:
				lineCol[eks][wai] = "square"



	#Phase 3: Draw the map based on the geography
	#Create a map filled with base colors
	mapPlaceholder = Image.new("RGBA", (Width, Height), (0, 255, 0))	# Placeholder Green: 
	mapPlaceholderL1 = Image.new("RGBA", (Width, Height), (0, 0, 0, 0))	#Completely invisible
	#Edges = [0] + [x * 32 + (200 - 16 * boardSize) for x in range(1,boardSize)] + [401]
	#YEdges = [0] + [y * 24 + (150 - 12 * boardSize) for y in range(1,boardSize)] + [301]
	for eks in range(0,boardSize):
		for wai in range(0,boardSize):
			tiletypes = ["grass","river_DL","river_DR","river_LR","river_UD","river_UL","river_UR","river_LR_bridge","river_UD_bridge"]
			#if arrayGeo[eks][wai] == "grass":	#grass
			#	mapPlaceholder.paste((0,255,0),(XEdges[eks],YEdges[wai],XEdges[eks+1],YEdges[wai+1]))
			if arrayGeo[eks][wai] in tiletypes:
				pasting = Image.open("{}/sprites/{}.png".format(folder,arrayGeo[eks][wai]))
				mapPlaceholder.paste(pasting,(eks * 32 + (Width//2 - 16 * boardSize), wai * 24 + (Height//2 - 12 * boardSize)))
				if arrayGeo[eks][wai] == "river_LR_bridge":
					pasting = Image.open("{}/sprites/river_LR_bridgerailings.png".format(folder))
					mapPlaceholderL1.paste(pasting,(eks * 32 + (Width//2 - 16 * boardSize), wai * 24 + (Height//2 - 12 * boardSize) - 12))
				elif arrayGeo[eks][wai] == "river_UD_bridge":
					pasting = Image.open("{}/sprites/river_UD_bridgerailings.png".format(folder))
					mapPlaceholderL1.paste(pasting,(eks * 32 + (Width//2 - 16 * boardSize), wai * 24 + (Height//2 - 12 * boardSize)))

	if rivStart[2] != "X":
		if rivStart[2] in ["l", "r"]:
			pasting = Image.open("{}/sprites/river_LRlong.png".format(folder))
		elif rivStart[2] in ["u", "d"]:
			pasting = Image.open("{}/sprites/river_UDlong.png".format(folder))
		mapPlaceholder.paste(pasting,(rivStart[0], rivStart[1]))
		if rivEnd[2] in ["l", "r"]:
			pasting = Image.open("{}/sprites/river_LRlong.png".format(folder))
		elif rivEnd[2] in ["u", "d"]:
			pasting = Image.open("{}/sprites/river_UDlong.png".format(folder))
		mapPlaceholder.paste(pasting,(rivEnd[0], rivEnd[1]))

	#Fill in the margins
	map1 = mapPlaceholder.load()
	mapL1 = mapPlaceholderL1.load()
	#Corners
	#margL = 200 - 16 * boardSize
	#margU = 150 - 12 * boardSize
	#margR = boardSize * 32 + margL
	#margD = boardSize * 24 + margU
	#mapPlaceholder.paste(map1[margL,margU],(0,0,margL,margU))
	#mapPlaceholder.paste(map1[margR-1,margU],(margR,0,400,margU))
	#mapPlaceholder.paste(map1[margL,margD-1],(0,margD,margL,300))
	#mapPlaceholder.paste(map1[margR-1,margD-1],(margR,margD,400,300))
	#Edges
	#for x in range(margL,margR):
	#	mapPlaceholder.paste(map1[x,margU],(x,0,x+1,margU))
	#	mapPlaceholder.paste(map1[x,margD-1],(x,margD,x+1,300))
	#for y in range(margU,margD):
	#	mapPlaceholder.paste(map1[margL,y],(0,y,margL,y+1))
	#	mapPlaceholder.paste(map1[margR-1,y],(margR,y,400,y+1))





	#Phase 4: Change placeholder colors to actual colors
	mapDetail = mapPlaceholder
	mapDetailL1 = mapPlaceholderL1
	#print(map0a[0,0])
	#if map0a[0,0] == (0, 255, 0, 255):
	#	print("Eureka!")
	grassGreens = [(55,111,55,255),(59,119,59,255),(63,127,63,255),(67,135,67,255),(71,143,71,255)]
	#waterPHBlues = [(0,225,225,255),(0,235,235,255),(0,245,245,255),(0,255,255,255)] # |, \, /, -
	waterBlues = [(55,111,167,255),(59,119,179,255),(63,127,191,255),(67,135,203,255),(71,143,215,255)]
	#waterBlues = [(62,125,188,255),(66,134,201,255),(71,143,215,255),(75,152,228,255),(80,161,242,255)]
	#waterBlues = [(55,84,167,255),(59,90,179,255),(63,96,191,255),(67,102,203,255),(71,108,215,255)]
	#dirtBrowns = [(55,41,27,255),(59,44,29,255),(63,47,31,255),(67,50,33,255),(71,53,35,255)]
	dirtBrowns = [(83,69,55,255),(89,74,59,255),(95,79,63,255),(101,84,67,255),(107,89,71,255)]
	stone1Grays = [(158,158,158,255), (164,164,164,255), (170,170,170,255), (176,176,176,255), (182,182,182,255)]
	stone2Grays = [(119,119,119,255), (123,123,123,255), (127,127,127,255), (131,131,131,255), (135,135,135,255)]
	stone3Grays = [(79,79,79,255), (82,82,82,255), (85,85,85,255), (88,88,88,255), (91,91,91,255)]
	stone4Grays = [(59,59,59,255), (61,61,61,255), (63,63,63,255), (65,65,65,255), (67,67,67,255)]

	for eks in range(0,Width):
		for wai in range(0,Height):
			if map1[eks,wai] == (0,255,0,255):	#Grass
				yeetGreen = random.randint(0,4)
				yeetHeight = min(random.randint(1,4),wai)
				mapDetail.putpixel((eks,wai),grassGreens[yeetGreen])
				for x in range(1,yeetHeight):
					mapDetail.putpixel((eks,wai-x),grassGreens[yeetGreen])

			if map1[eks,wai] == (0,255,255,255):	#River, flowing -
				yeetBlue = random.randint(0,4)
				yeetFlow = min(random.randint(1,4),Width-eks)
				mapDetail.putpixel((eks,wai),waterBlues[yeetBlue])
				for x in range(1,yeetFlow):
					if map1[eks+x,wai] == (0,255,255,255):
						mapDetail.putpixel((eks+x,wai),waterBlues[yeetBlue])
			if map1[eks,wai] == (0,225,225,255):	#River, flowing |
				yeetBlue = random.randint(0,4)
				yeetFlow = min(random.randint(1,3),Height-wai)
				mapDetail.putpixel((eks,wai),waterBlues[yeetBlue])
				for x in range(1,yeetFlow):
					if map1[eks,wai+x] == (0,225,225,255):
						mapDetail.putpixel((eks,wai+x),waterBlues[yeetBlue])
			if map1[eks,wai] == (0,235,235,255):	#River, flowing \
				yeetBlue = random.randint(0,4)
				yeetFlow = min(random.randint(1,4),Width-eks)
				mapDetail.putpixel((eks,wai),waterBlues[yeetBlue])
				y = 0
				for x in range(1,yeetFlow):
					if random.random() < 3/4:
						y += 1
					if wai+y < Height:
						if map1[eks+x,wai+y] == (0,235,235,255):
							mapDetail.putpixel((eks+x,wai+y),waterBlues[yeetBlue])
			if map1[eks,((Height-1)-wai)] == (0,245,245,255):	#River, flowing /
				yeetBlue = random.randint(0,4)
				yeetFlow = min(random.randint(1,4),Width-eks)
				mapDetail.putpixel((eks,((Height-1)-wai)),waterBlues[yeetBlue])
				y = 0
				for x in range(1,yeetFlow):
					if random.random() < 3/4:
						y -= 1
					if wai+y >= 0:
						if map1[eks+x,((Height-1)-wai)+y] == (0,245,245,255):
							mapDetail.putpixel((eks+x,((Height-1)-wai)+y),waterBlues[yeetBlue])

			if map1[eks,wai] == (255,127,0,255):	#Riverbank
				yeetColor = random.randint(0,4)
				mapDetail.putpixel((eks,wai),dirtBrowns[yeetColor])
				yeetSize = random.randint(0,16)
				ecks = random.randint(-1,1)
				huai = random.randint(-1,1)
				while yeetSize > 0 and eks+ecks in range(0,Width) and wai+huai in range(0,Height):
					if map1[eks+ecks,wai+huai] == (255,127,0,255):
						mapDetail.putpixel((eks+ecks,wai+huai),dirtBrowns[yeetColor])
						ecks += random.randint(-1,1)
						huai += random.randint(-1,1)
					else:
						ecks = random.randint(-1,1)
						huai = random.randint(-1,1)
					yeetSize -= 1

			if map1[eks,wai] == (170,170,170,255):	#Building Grays
				yeetColor = random.randint(0,4)
				mapDetail.putpixel((eks,wai),stone1Grays[yeetColor])
			if map1[eks,wai] == (127,127,127,255):
				yeetColor = random.randint(0,4)
				mapDetail.putpixel((eks,wai),stone2Grays[yeetColor])
			if map1[eks,wai] == (85,85,85,255):
				yeetColor = random.randint(0,4)
				mapDetail.putpixel((eks,wai),stone3Grays[yeetColor])
			if map1[eks,wai] == (63,63,63,255):
				yeetColor = random.randint(0,4)
				mapDetail.putpixel((eks,wai),stone4Grays[yeetColor])

			if mapL1[eks,wai] == (170,170,170,255):	#Building Grays - Layer 1
				yeetColor = random.randint(0,4)
				mapDetailL1.putpixel((eks,wai),stone1Grays[yeetColor])
			if mapL1[eks,wai] == (127,127,127,255):
				yeetColor = random.randint(0,4)
				mapDetailL1.putpixel((eks,wai),stone2Grays[yeetColor])
			if mapL1[eks,wai] == (85,85,85,255):
				yeetColor = random.randint(0,4)
				mapDetailL1.putpixel((eks,wai),stone3Grays[yeetColor])
			if mapL1[eks,wai] == (63,63,63,255):
				yeetColor = random.randint(0,4)
				mapDetailL1.putpixel((eks,wai),stone4Grays[yeetColor])

	#Phase 5: Get the map image with the final colors
	package = {'floor' : mapDetail, 'layer1' : mapDetailL1, 'collision' : arrCol, 'linecol' : lineCol}
	return package

if False:	#Set to True when testing
	file_path = os.path.abspath(__file__)
	folder = os.path.dirname(file_path)

	map1 = generate(24)['floor']
	map1.save("{}/stuff/sprites/aaatestmap.png".format(folder))
	print("Done!")
