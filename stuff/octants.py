#!/usr/bin/env python3
import math

def find_octant(x1, y1, x2, y2):
	dx = x1 - x2
	dy = y1 - y2
	if dx == 0:
		if dy > 0:
			return 6
		elif dy < 0:
			return 2
		else:
			return None
	angle = math.atan(-dy/dx)
	octant = round(angle/(math.tau/8))
	if dx < 0:
		octant += 4
	octant %= 8
	return octant
	
for y in range(-10, 10):
	for x in range(-10, 10):
		octant = find_octant(x, y, 2, 5)
		if octant is None:
			octant = " "
		print(octant, end=" ")
	print()
