import random, math, os
import ffmpeg
from PIL import Image

file_path = os.path.abspath(__file__)
folder = os.path.dirname(file_path)

if not os.path.isdir("{}/frames".format(folder)):
	os.makedirs("{}/frames".format(folder))
for frame in os.listdir("{}/frames".format(folder)):
	os.remove("{}/frames/{}".format(folder, frame))

def distance(sx, sy, gx, gy):
	dx = abs(gx - sx)
	dy = abs(gy - sy) * 4/3
	return math.sqrt(dx**2 + dy**2)

def angdiff(sx, sy, sa, gx, gy):
	dx = gx - sx
	dy = (sy - gy) * 4/3
	if dx == 0:
		if dy < 0:
			ga = -math.tau / 4
		elif dy > 0:
			ga = math.tau / 4
		else:
			ga = sa
	else:
		ga = math.atan(dy/dx)
		if dx < 0:
			ga += math.tau / 2
	ans = (ga - sa) % math.tau
	if ans > math.tau / 2:
		ans -= math.tau
	return ans

class Sprite:
	def __init__(self, image, x = 0, y = 0, image_x = 0, image_y = 0):
		self.image = image
		self.x = x
		self.y = y
		self.image_x = image_x
		self.image_y = image_y

	def draw_on(self, frame):
		frame.paste(self.image, (round(self.x+self.image_x), round(self.y+self.image_y)), self.image)
	
class Soldier(Sprite):
	def __init__(self, x = 0, y = 0, angle = 0, intention = [], action = "", progress = 0):
		self.x = x
		self.y = y
		self.angle = angle
		self.intention = intention
		self.action = action
		self.progress = progress

		self.image_x = -12
		self.image_y = -28
		
		self.gotorandomsX = 200
		self.gotorandomsY = 150
		self.gotorandomsDist = 10

	def update_sprite(self):
		frontarm_sheet = Image.open("{}/sprites/soldiernaked_frontarm_straightsword_16.png".format(folder))
		frontleg_sheet = Image.open("{}/sprites/soldiernaked_frontleg_16.png".format(folder))
		body_sheet = Image.open("{}/sprites/soldiernaked_body_16.png".format(folder))
		backarm_sheet = Image.open("{}/sprites/soldiernaked_backarm_straightsword_16.png".format(folder))
		backleg_sheet = Image.open("{}/sprites/soldiernaked_backleg_16.png".format(folder))
		
		facing = round(self.angle / (math.tau / 16) + 7.5) % 16

		frontarm = frontarm_sheet.crop((facing * 24, 0, (facing + 1) * 24, 32))
		frontleg = frontleg_sheet.crop((facing * 24, 0, (facing + 1) * 24, 32))
		body = body_sheet.crop((facing * 24, 0, (facing + 1) * 24, 32))
		backarm = backarm_sheet.crop((facing * 24, 0, (facing + 1) * 24, 32))
		backleg = backleg_sheet.crop((facing * 24, 0, (facing + 1) * 24, 32))

		image = Image.new("RGBA", (24, 32))

		image.paste(backleg, (0, 0), backleg)
		image.paste(backarm, (0, 0), backarm)
		image.paste(body, (0, 0), body)
		image.paste(frontleg, (0, 0), frontleg)
		image.paste(frontarm, (0, 0), frontarm)
		
		self.image = image

	def think(self):
		if self.intention[0] == "gotorandoms":
			ang = angdiff(self.x, self.y, self.angle, self.gotorandomsX, self.gotorandomsY)
			if abs(ang) > math.tau / 32:
				if ang < 0:
					self.action = "turnR"
				else:
					self.action = "turnL"
			else:
				if distance(self.x, self.y, self.gotorandomsX, self.gotorandomsY) < self.gotorandomsDist:
					self.gotorandomsX = random.random() * 400
					self.gotorandomsY = random.random() * 300
				else:
					self.action = "walk"

	def act(self):
		if self.action == "turnL":
			self.angle += math.tau / 32
		if self.action == "turnR":
			self.angle -= math.tau / 32
		if self.action == "walk":
			# Update to use SPD
			self.x += math.cos(self.angle)
			self.y -= math.sin(self.angle) * 0.75

frames = 0
soldiers = []
for i in range(0, 50):
	soldiers.append(Soldier(random.randrange(0, 400), random.randrange(0, 300), random.random() * math.tau, ["gotorandoms"]))

sprites = soldiers + []

x_sprite = Image.open("{}/sprites/x.png".format(folder))

while frames < 500:
	frame = Image.new("RGBA", (400, 300), (43, 130, 51))
	#print("frame {}".format(frames))
	for soldier in soldiers:
		#AI
		soldier.think()
		soldier.act()
		
		soldier.update_sprite()
		frame.paste(x_sprite, (round(soldier.gotorandomsX) - 4, round(soldier.gotorandomsY) - 4), x_sprite)

	sprite_order = sorted(sprites, key=lambda sprite: sprite.y)

	for sprite in sprite_order:
		sprite.draw_on(frame)


	frame.save("{}/frames/{}.png".format(folder, frames))
	frames += 1

stream = ffmpeg.input("{}/frames/%d.png".format(folder), r=20)
stream = ffmpeg.output(stream, "{}/output.mp4".format(folder))
ffmpeg.run(stream)
