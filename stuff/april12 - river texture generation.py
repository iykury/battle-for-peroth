import random, math, os
import ffmpeg
from PIL import Image

file_path = os.path.abspath(__file__)
folder = os.path.dirname(file_path)

if not os.path.isdir("{}/frames".format(folder)):
	os.makedirs("{}/frames".format(folder))
for frame in os.listdir("{}/frames".format(folder)):
	os.remove("{}/frames/{}".format(folder,frame))

#my own sign function, because fuck it math.copysign is for horrible people
def sign(x):
	if x > 0:
		return 1
	elif x < 0:
		return -1
	else:
		return 0
def signPos(x):
	if x >= 0:
		return 1
	elif x < 0:
		return -1
def signNeg(x):
	if x > 0:
		return 1
	elif x <= 0:
		return -1

boardSize = 10
#8, 10, 12

#Phase 1: Generate a map
arrayGeo = [["grass" for x in range(0,boardSize)] for y in range(0,boardSize)]	#Right now, it just fills the entire board with grass

#Generate river
if random.random() >= 0: #1/2:	#Will this map have a river? 1/2
	yeetRivStartDir = random.random()
	rivMode = 1
	if yeetRivStartDir < 1/4:	#Will it be drawn beginning from the top, or the left? / Starts from left
		rivx = 0
		rivy = random.randint(0,boardSize/2) + random.randint(0,boardSize/2)
		while rivx < boardSize and rivy in range(0,boardSize):
			yeetRiv = random.random()
			if rivMode == 1:
				if yeetRiv < 1/8:	#Curves up
					arrayGeo[rivx][rivy] = "river_UL"
					rivMode = 0
					rivy -= 1
				elif yeetRiv >= 7/8:	#Curves down
					arrayGeo[rivx][rivy] = "river_DL"
					rivMode = 2
					rivy += 1
				else:	#Goes straight
					arrayGeo[rivx][rivy] = "river_LR"
					rivx += 1
			elif rivMode == 0:	#If going up
				if yeetRiv < 1/2:	#Goes straight
					arrayGeo[rivx][rivy] = "river_UD"
					rivy -= 1
				else:	#Curves back right
					arrayGeo[rivx][rivy] = "river_DR"
					rivMode = 1
					rivx += 1
			elif rivMode == 2:	#If going down
				if yeetRiv < 1/2:	#Goes straight
					arrayGeo[rivx][rivy] = "river_UD"
					rivy += 1
				else:	#Curves back right
					arrayGeo[rivx][rivy] = "river_UR"
					rivMode = 1
					rivx += 1
	elif yeetRivStartDir < 1/2:	#Starts from top
		rivx = random.randint(0,boardSize/2) + random.randint(0,boardSize/2) 
		rivy = 0
		while rivy < boardSize and rivx in range(0,boardSize):
			yeetRiv = random.random()
			if rivMode == 1:
				if yeetRiv < 1/8:	#Curves left
					arrayGeo[rivx][rivy] = "river_UL"
					rivMode = 0
					rivx -= 1
				elif yeetRiv >= 7/8:	#Curves right
					arrayGeo[rivx][rivy] = "river_UR"
					rivMode = 2
					rivx += 1
				else:	#Goes straight
					arrayGeo[rivx][rivy] = "river_UD"
					rivy += 1
			elif rivMode == 0:	#If going left
				if yeetRiv < 1/2:	#Goes straight
					arrayGeo[rivx][rivy] = "river_LR"
					rivx -= 1
				else:	#Curves back down
					arrayGeo[rivx][rivy] = "river_DR"
					rivMode = 1
					rivy += 1
			elif rivMode == 2:	#If going right
				if yeetRiv < 1/2:	#Goes straight
					arrayGeo[rivx][rivy] = "river_LR"
					rivx += 1
				else:	#Curves back down
					arrayGeo[rivx][rivy] = "river_DL"
					rivMode = 1
					rivy += 1
	elif yeetRivStartDir < 3/4:	#Starts from right
		rivx = boardSize - 1
		rivy = random.randint(0,boardSize/2) + random.randint(0,boardSize/2)
		while rivx >= 0 and rivy in range(0,boardSize):
			yeetRiv = random.random()
			if rivMode == 1:
				if yeetRiv < 1/8:	#Curves up
					arrayGeo[rivx][rivy] = "river_UR"
					rivMode = 0
					rivy -= 1
				elif yeetRiv >= 7/8:	#Curves down
					arrayGeo[rivx][rivy] = "river_DR"
					rivMode = 2
					rivy += 1
				else:	#Goes straight
					arrayGeo[rivx][rivy] = "river_LR"
					rivx -= 1
			elif rivMode == 0:	#If going up
				if yeetRiv < 1/2:	#Goes straight
					arrayGeo[rivx][rivy] = "river_UD"
					rivy -= 1
				else:	#Curves back right
					arrayGeo[rivx][rivy] = "river_DL"
					rivMode = 1
					rivx -= 1
			elif rivMode == 2:	#If going down
				if yeetRiv < 1/2:	#Goes straight
					arrayGeo[rivx][rivy] = "river_UD"
					rivy += 1
				else:	#Curves back right
					arrayGeo[rivx][rivy] = "river_UL"
					rivMode = 1
					rivx -= 1
	else:	#Starts from bottom
		rivx = random.randint(0,boardSize/2) + random.randint(0,boardSize/2) 
		rivy = boardSize - 1
		while rivy >= 0 and rivx in range(0,boardSize):
			yeetRiv = random.random()
			if rivMode == 1:
				if yeetRiv < 1/8:	#Curves left
					arrayGeo[rivx][rivy] = "river_DL"
					rivMode = 0
					rivx -= 1
				elif yeetRiv >= 7/8:	#Curves right
					arrayGeo[rivx][rivy] = "river_DR"
					rivMode = 2
					rivx += 1
				else:	#Goes straight
					arrayGeo[rivx][rivy] = "river_UD"
					rivy -= 1
			elif rivMode == 0:	#If going left
				if yeetRiv < 1/2:	#Goes straight
					arrayGeo[rivx][rivy] = "river_LR"
					rivx -= 1
				else:	#Curves back down
					arrayGeo[rivx][rivy] = "river_UR"
					rivMode = 1
					rivy -= 1
			elif rivMode == 2:	#If going right
				if yeetRiv < 1/2:	#Goes straight
					arrayGeo[rivx][rivy] = "river_LR"
					rivx += 1
				else:	#Curves back down
					arrayGeo[rivx][rivy] = "river_UL"
					rivMode = 1
					rivy -= 1

#Phase 2: Draw the map based on the geography
#Create a map filled with base colors
mapPlaceholder = Image.new("RGBA", (400, 300), (0, 0, 0))	# Placeholder Black: Nothing
XEdges = [0] + [x * 32 + (200 - 16 * boardSize) for x in range(1,boardSize)] + [401]
YEdges = [0] + [y * 24 + (150 - 12 * boardSize) for y in range(1,boardSize)] + [301]
for eks in range(0,boardSize):
	for wai in range(0,boardSize):
		tiletypes = ["grass","river_DL","river_DR","river_LR","river_UD","river_UL","river_UR"]
		if arrayGeo[eks][wai] == "grass":	#grass
			mapPlaceholder.paste((0,255,0),(XEdges[eks],YEdges[wai],XEdges[eks+1],YEdges[wai+1]))
		if arrayGeo[eks][wai] in tiletypes:
			pasting = Image.open("{}/sprites/{}.png".format(folder,arrayGeo[eks][wai]))
			mapPlaceholder.paste(pasting,(eks * 32 + (200 - 16 * boardSize), wai * 24 + (150 - 12 * boardSize)))

#Fill in the margins
map1 = mapPlaceholder.load()
#Corners
margL = 200 - 16 * boardSize
margU = 150 - 12 * boardSize
margR = boardSize * 32 + margL
margD = boardSize * 24 + margU
mapPlaceholder.paste(map1[margL,margU],(0,0,margL,margU))
mapPlaceholder.paste(map1[margR-1,margU],(margR,0,400,margU))
mapPlaceholder.paste(map1[margL,margD-1],(0,margD,margL,300))
mapPlaceholder.paste(map1[margR-1,margD-1],(margR,margD,400,300))
#Edges
for x in range(margL,margR):
	mapPlaceholder.paste(map1[x,margU],(x,0,x+1,margU))
	mapPlaceholder.paste(map1[x,margD-1],(x,margD,x+1,300))
for y in range(margU,margD):
	mapPlaceholder.paste(map1[margL,y],(0,y,margL,y+1))
	mapPlaceholder.paste(map1[margR-1,y],(margR,y,400,y+1))





#Change placeholder colors to actual colors
mapDetail = mapPlaceholder
#print(map0a[0,0])
#if map0a[0,0] == (0, 255, 0, 255):
#	print("Eureka!")
grassGreens = [(55,111,55,255),(59,119,59,255),(63,127,63,255),(67,135,67,255),(71,143,71,255)]
#waterPHBlues = [(0,225,225,255),(0,235,235,255),(0,245,245,255),(0,255,255,255)] # |, \, /, -
waterBlues = [(55,111,167,255),(59,119,179,255),(63,127,191,255),(67,135,203,255),(71,143,215,255)]
#waterBlues = [(62,125,188,255),(66,134,201,255),(71,143,215,255),(75,152,228,255),(80,161,242,255)]
#waterBlues = [(55,84,167,255),(59,90,179,255),(63,96,191,255),(67,102,203,255),(71,108,215,255)]
#dirtBrowns = [(55,41,27,255),(59,44,29,255),(63,47,31,255),(67,50,33,255),(71,53,35,255)]
dirtBrowns = [(83,69,55,255),(89,74,59,255),(95,79,63,255),(101,84,67,255),(107,89,71,255)]

for eks in range(0,400):
	for wai in range(0,300):
		if map1[eks,wai] == (0,255,0,255):	#Grass
			yeetGreen = random.randint(0,4)
			yeetHeight = min(random.randint(1,4),wai)
			mapDetail.putpixel((eks,wai),grassGreens[yeetGreen])
			for x in range(1,yeetHeight):
				mapDetail.putpixel((eks,wai-x),grassGreens[yeetGreen])

		if map1[eks,wai] == (0,255,255,255):	#River, flowing -
			yeetBlue = random.randint(0,4)
			yeetFlow = min(random.randint(1,4),400-eks)
			mapDetail.putpixel((eks,wai),waterBlues[yeetBlue])
			for x in range(1,yeetFlow):
				if map1[eks+x,wai] == (0,255,255,255):
					mapDetail.putpixel((eks+x,wai),waterBlues[yeetBlue])
		if map1[eks,wai] == (0,225,225,255):	#River, flowing |
			yeetBlue = random.randint(0,4)
			yeetFlow = min(random.randint(1,3),300-wai)
			mapDetail.putpixel((eks,wai),waterBlues[yeetBlue])
			for x in range(1,yeetFlow):
				if map1[eks,wai+x] == (0,225,225,255):
					mapDetail.putpixel((eks,wai+x),waterBlues[yeetBlue])
		if map1[eks,wai] == (0,235,235,255):	#River, flowing \
			yeetBlue = random.randint(0,4)
			yeetFlow = min(random.randint(1,4),400-eks)
			mapDetail.putpixel((eks,wai),waterBlues[yeetBlue])
			y = 0
			for x in range(1,yeetFlow):
				if random.random() < 3/4:
					y += 1
				if wai+y < 300:
					if map1[eks+x,wai+y] == (0,235,235,255):
						mapDetail.putpixel((eks+x,wai+y),waterBlues[yeetBlue])
		if map1[eks,(299-wai)] == (0,245,245,255):	#River, flowing /
			yeetBlue = random.randint(0,4)
			yeetFlow = min(random.randint(1,4),400-eks)
			mapDetail.putpixel((eks,(299-wai)),waterBlues[yeetBlue])
			y = 0
			for x in range(1,yeetFlow):
				if random.random() < 3/4:
					y -= 1
				if wai+y >= 0:
					if map1[eks+x,(299-wai)+y] == (0,245,245,255):
						mapDetail.putpixel((eks+x,(299-wai)+y),waterBlues[yeetBlue])

		if map1[eks,wai] == (255,127,0,255):	#Riverbank
			yeetColor = random.randint(0,4)
			mapDetail.putpixel((eks,wai),dirtBrowns[yeetColor])
			yeetSize = random.randint(0,16)
			ecks = random.randint(-1,1)
			huai = random.randint(-1,1)
			while yeetSize > 0 and eks+ecks in range(0,400) and wai+huai in range(0,300):
				if map1[eks+ecks,wai+huai] == (255,127,0,255):
					mapDetail.putpixel((eks+ecks,wai+huai),dirtBrowns[yeetColor])
					ecks += random.randint(-1,1)
					huai += random.randint(-1,1)
				else:
					ecks = random.randint(-1,1)
					huai = random.randint(-1,1)
				yeetSize -= 1


#Phase 2.3: Add overlays
mapOverlays = mapDetail
mapgrid = Image.open("{}/sprites/overlay_{}board.png".format(folder,boardSize))
mapOverlays.paste(mapgrid,(0,0),mapgrid)

#Phase 2.4: Get the map image with the final colors
mapFinal = mapOverlays.resize((800,600))
mapFinal.save("{}/frames/map.png".format(folder))

print("Done!")