#!/usr/bin/env python3
import sys, os, math
from PIL import Image

file_path = os.path.abspath(__file__)
folder = os.path.dirname(file_path)

width = int(sys.argv[1])
height = int(sys.argv[2])
message = sys.argv[3]
output = os.path.abspath(sys.argv[4])
if len(sys.argv) > 5:
	font = sys.argv[5]
else:
	font = "default"

line_height = x_offset = y_offset = 0
case = None
os.chdir("{}/{}".format(folder, font))
with open("properties") as f:
	lines = f.readlines()
	for line in lines:
		l = line.strip("\n").split("=")
		if l[0] == "height":
			line_height = int(l[1])
		elif l[0] == "case":
			case = l[1]
		elif l[0] == "x_offset":
			x_offset = int(l[1])
		elif l[0] == "y_offset":
			y_offset = int(l[1])

image = Image.new("RGBA", (width, height))
x = 0
y = 0
for char in message:
	if char == "\n":
		x = 0
		y += line_height
	else:
		if case == "upper":
			char = char.upper()
		elif case == "lower":
			char = char.lower()

		char_filename = "{:04x}.png".format(ord(char)) #Get character codepoint in hex and pad it with zeroes
		
		if os.path.exists(char_filename):
			char_image = Image.open(char_filename)
		else:
			char_image = Image.open("tofu.png")
		
		image.paste(char_image, (x + x_offset, y + y_offset), char_image)
		x += char_image.width
	print(char, end = "", flush = True)
print()
image.save(output)
