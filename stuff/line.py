import math

#<the actual code>
def distance(pointA, pointB):
	return math.sqrt((pointA[0] - pointB[0]) ** 2 + (pointA[1] - pointB[1]) ** 2)

def intersects(arrCol, pointA, pointB):
	#Create a 2d list of Nones with the size as arrCol
	arrInt = [] 
	for i in range(0, len(arrCol)):
		arrInt.append([None] * len(arrCol[0]))
	
	#Find slope and y-intercept of line
	m1 = (pointB[1] - pointA[1]) / (pointB[0] - pointA[0]) #m = rise/run
	b1 = pointA[1] - m1 * pointA[0] #y-mx = b

	for y in range(0, len(arrCol)):
		for x in range(0, len(arrCol[0])):
			if "circle" in arrCol[x][y]:
				#Slope and y-intercept of the line that passes through the middle of the tile
				#and is perpendicular to the line from point A to point B
				m2 = -1 / m1
				b2 = y - m2 * x
					#Where the two lines intersect is the closest point on the line to the tile
				closestX = (b1 - b2) / (m2 - m1)
				closestY = m2 * closestX + b2
				closest = (closestX, closestY) #The closest point on the line to (x, y)
				
				#If the closest point on the line falls outside the line segment
				if closest[0] < min(pointA[0], pointB[0]) or closest[0] > max(pointA[0], pointB[0]) or closest[1] < min(pointA[1], pointB[1]) or closest[1] > max(pointA[1], pointB[1]):
					#If point B is closer than point A, then point B is the closest point
					if distance(pointB, (x, y)) < distance(pointA, (x, y)):
						closest = pointB
					#Otherwise, point A is the closest
					else:
						closest = pointA
					if distance(closest, (x, y)) <= 0.5:
						return True
			elif arrCol[x][y] not in ["clear", "gap"]:
				#Step 1: Check if tile is within the segment's bounding box
				if x >= math.floor(min(pointA[0], pointB[0])) and x + 1 <= math.ceil(max(pointA[0], pointB[0])):# and y >= math.floor(min(pointA[1], pointB[1])) and y + 1 <= math.ceil(max(pointA[1], pointB[1])):

					#Step 2: Evaluate y-axis of points on slope directly above/below the tile, and calculate differences between those points and top of tile
					leftEdge = max(min(pointA[0], pointB[0]), x) * m1 + b1 - y
					rightEdge = min(max(pointA[0], pointB[0]), x + 1) * m1 + b1 - y

					#Step 3: Check all possible signs of collision.  First two check to see if the line intercepts either the left or right side of the tile, second two check if the line intercepts both the top and bottom sides.
					if (leftDif >= 0 and leftDif <= 1) or (rightDif >= 0 and rightDif <= 1) or (leftDif < 0 and rightDif > 1) or (leftDif > 1 and rightDif < 0):
						return True

	return False
#</the actual code>

arrCol = []

for i in range(0, 3):
	arrCol.append(["empty"] * 3)

arrCol[0][2] = "circle"
arrCol[1][1] = "square"

#Intersects circle
#arrInt = findIntersections(arrCol, (0.636, 2.4), (-0.364, 0.2))

#Intersects square
arrInt = ifIntersect(arrCol, (0.682, 1.8), (-0.227, -0.2))

#Intersects neither 
#arrInt = findIntersections(arrCol, (0.636, 2.0), (-0.273, 0.0))

for y in range(3):
	for x in range(3):
		if arrCol[x][y] == "empty":
			print(" ", end = "", flush = True)
		elif arrCol[x][y] == "square":
			print("#", end = "", flush = True)
		elif arrCol[x][y] == "circle":
			print("O", end = "", flush = True)
	print()
print("======")
for y in range(3):
	for x in range(3):
		if arrInt[x][y]:
			print("X", end = "", flush = True)
		else:
			print(" ", end = "", flush = True)
	print()
