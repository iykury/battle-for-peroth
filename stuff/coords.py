import string

#pseudo chess algebraic notation
def coords(pcan):
    x_str = ""
    y_str = ""

    if pcan[0] in string.ascii_letters:
        x_first = True
    else:
        x_first = False

    for char in pcan:
        if char in string.ascii_letters:
            if x_str == "":
                x_str = char.lower()
            else:
                return None
        elif char in string.digits:
            if x_str == "" or x_first:
                y_str += char
            else:
                return None

    x = ord(x_str) - 97
    y = int(y_str) - 1

    return (x, y)

while True:
    pcan = input("> ")
    print(coords(pcan))
