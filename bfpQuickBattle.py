import asyncio, random, math, os, discord, string
from PIL import Image, ImageChops
import battle, prep, content

games = []

class Game:
	def __init__(self, channels, rules = {}):
		self.teams = []
		self.lobbies = []
		self.players = []
		self.pawns = []
		for channel in channels:
			self.teams.append(Team(channel))
			if not channel.server.qb_lobby in self.lobbies:
				self.lobbies.append(channel.server.qb_lobby)

		#All of these should be determined in the lobby
		self.pawnsPerPlayer = rules.get('pawns per player', 4)
		self.redFog = rules.get('red fog', False)
		self.turnRule = 3

		self.Begin = False
		self.Countdown = 10

		colors = ["white", "black", "red", "blue", "yellow", "green", "purple", "orange"]
		Id = 1
		for team in self.teams:
			for player in team.players:
				if len(colors) == 0:
					colors = ["white", "black", "red", "blue", "yellow", "green", "purple", "orange"]
				player.color = random.choice(colors)
				colors.remove(player.color)
				player.team = team
				player.Id = Id
				Id += 1
				player.inventory = []
				player.Points = math.ceil(10/3 * self.pawnsPerPlayer)
				# v Testing
				#player.inventory.append(content.smallMace())
				#player.inventory.append(content.smallMace())
				#player.inventory.append(content.smallMace())
				#player.inventory.append(content.healthPotion())
				# ^ Testing
				player.Ready = False
				self.players.append(player)

		for team in self.teams:
			for player in team.players:
				player.pawns = []
				for i in range(self.pawnsPerPlayer):
					testName = random.choice(["Alpha", "Bravo", "Charlie", "Delta", "Echo", "Foxtrot", "Golf", "Hotel", "India", "Juliet", "Kilo", "Lima", "Mike", "November", "Oscar", "Papa", "Quebec", "Romeo", "Sierra", "Tango", "Uniform", "Victor", "Whiskey", "X-Ray", "Yankee", "Zulu", "Sleve", "Mcdichael", "Willie", "Dustice", "Onson", "Sweemey", "Jeromy", "Gride", "Daryl", "Archideld", "Scott", "Furcotte", "Rey", "Mcsriff", "Dean", "Wesrey", "Glenallen", "Mixon", "Truk", "Mario", "Mcrlwain", "Dwigt", "Rortugal", "Raul", "Chamgerlain", "Tim", "Sandaele", "Kevin", "Nogilny", "Karl", "Dandleton", "Tony", "Smehrik", "Sernandez", "Bobson", "Dugnutt", "Todd", "Bonzalez", "Nert", "Bisels", "Pott", "Korhil", "Kenn", "Nitvarn", "Am", "O'Erson", "Fergit", "Hote", "Snarry", "Shitwon", "Coll", "Bitzron", "Bobs", "Peare", "Lood", "Janglosti", "Renly", "Mlynren", "Taenis", "Tellron", "Ceynei", "Doober", "Marnel", "Hary", "Hom", "Wapko", "Dony", "Olerberz", "Odood", "Jorgeudey", "Gin", "Ginlons", "Gary", "Banps", "Wob", "Wonkoz", "Jaris", "Forta", "Tanny", "Mlitnirt", "Erl", "Jivliiz", "Hudgyn", "Sasdarl", "Lenn", "Wobses", "Fraven", "Pooth", "Dan", "Boyo", "Rarr", "Dick", "Yans", "Loovensan", "Dorse", "Hintline", "Mob", "Welronz", "Gamo", "Bannoe", "Rodylar", "Tenpe", "Laob", "Al", "Swermirstz", "Varlin", "Genmist", "Jinneil", "Robenko", "Alex", "Jeff", "Ngeng", "Hxck"])
					newPawn = content.Pawn(self, player, testName)
					newPawn.Id = len(self.pawns)
					newPawn.id_Player = len(player.pawns)

					player.pawns.append(newPawn)
					self.pawns.append(newPawn)

		prep.preps.append(prep.Prep(self.teams, self.lobbies, self.players, self.pawns, rules))
		#battle.battles.append(battle.Battle(self.teams, self.lobbies, self.players, self.pawns, rules))

	async def on_message(self, message):
		for p in prep.preps:
			await p.on_message(message)
		for b in battle.battles:
			await b.on_message(message)

class Team:
	def __init__(self, channel):
		self.channel = channel
		self.players = channel.players

	def noPawns(self):
		result = 0
		for p in self.players:
			result += len(p.pawns)
		return result