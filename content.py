import asyncio, random, math, os, discord, string
from PIL import Image, ImageChops

#section 1 math (mathtop)

def nrt(x, y = 2):
	return x ** (1/y)

def boardsizeCal(x):
	s = 2 ** math.floor(math.log(x * 2/3, 4))
	return math.floor(x / s + 3 * s)

def boardsizeCal_unrounded(x):
	s = 2 ** math.log(x * 2/3, 4)
	return (x / s + 3 * s)

def distance(pointA, pointB):
	return math.sqrt((pointA[0] - pointB[0]) ** 2 + (pointA[1] - pointB[1]) ** 2)

def gridDist(pointA, pointB):
	return max(abs(pointA[0] - pointB[0]), abs(pointA[1] - pointB[1]))

def clearline(arrCol, lineCol, pointA, pointB, skew = True):
	#Adjust point inputs
	if skew:
		pointA[0] += 0.5
		pointA[1] += 0.5
		pointB[0] += 0.5
		pointB[1] += 0.5

	#Create a 2d list of Nones with the size as arrCol
	arrInt = [] 
	for i in range(0, len(arrCol)):
		arrInt.append([None] * len(arrCol[0]))
	
	#Find slope and y-intercept of line
	if pointB[0] - pointA[0] == 0:
		m1 = 9999 #a temporary fix
	else:
		m1 = (pointB[1] - pointA[1]) / (pointB[0] - pointA[0]) #m = rise/run
	b1 = pointA[1] - m1 * pointA[0] #y-mx = b

	for y in range(0, len(arrCol)):
		for x in range(0, len(arrCol[0])):
			if lineCol[x][y] == "circle" and arrCol[x][y] not in ["clear", "gap"]:
				#Slope and y-intercept of the line that passes through the middle of the tile
				#and is perpendicular to the line from point A to point B
				m2 = -1 / m1
				b2 = y - m2 * x
					#Where the two lines intersect is the closest point on the line to the tile
				closestX = (b1 - b2) / (m2 - m1)
				closestY = m2 * closestX + b2
				closest = (closestX, closestY) #The closest point on the line to (x, y)
				
				#If the closest point on the line falls outside the line segment
				if closest[0] < min(pointA[0], pointB[0]) or closest[0] > max(pointA[0], pointB[0]) or closest[1] < min(pointA[1], pointB[1]) or closest[1] > max(pointA[1], pointB[1]):
					#If point B is closer than point A, then point B is the closest point
					if distance(pointB, (x, y)) < distance(pointA, (x, y)):
						closest = pointB
					#Otherwise, point A is the closest
					else:
						closest = pointA
					if distance(closest, (x, y)) <= 0.5:
						return False
			elif lineCol[x][y] == "square" and arrCol[x][y] not in ["clear", "gap"]:
				#Step 1: Check if tile is within the segment's bounding box
				if x >= math.floor(min(pointA[0], pointB[0])) and x + 1 <= math.ceil(max(pointA[0], pointB[0])):# and y >= math.floor(min(pointA[1], pointB[1])) and y + 1 <= math.ceil(max(pointA[1], pointB[1])):

					#Step 2: Evaluate y-axis of points on slope directly above/below the tile, and calculate differences between those points and top of tile
					leftEdge = max(min(pointA[0], pointB[0]), x) * m1 + b1 - y
					rightEdge = min(max(pointA[0], pointB[0]), x + 1) * m1 + b1 - y

					#Step 3: Check all possible signs of collision.  First two check to see if the line intercepts either the left or right side of the tile, second two check if the line intercepts both the top and bottom sides.
					if (leftDif >= 0 and leftDif <= 1) or (rightDif >= 0 and rightDif <= 1) or (leftDif < 0 and rightDif > 1) or (leftDif > 1 and rightDif < 0):
						return False
	return True

def octogonal(deltaX, deltaY):
	return (deltaX == 0 or deltaY == 0 or abs(deltaX) == abs(deltaY))

def drawText(image, message, font, xPos, yPos, color = (255,255,255,255)):
	file_path = os.path.abspath(__file__)
	folder = os.path.dirname(file_path)

	line_height = x_offset = y_offset = 0
	case = None
	with open("{}/fonts/{}/properties".format(folder, font)) as f:
		lines = f.readlines()
		for line in lines:
			l = line.strip("\n").split("=")
			if l[0] == "height":
				line_height = int(l[1])
			elif l[0] == "case":
				case = l[1]
			elif l[0] == "x_offset":
				x_offset = int(l[1])
			elif l[0] == "y_offset":
				y_offset = int(l[1])

	x = 0
	y = 0
	for char in message:
		if char == "\n":
			x = 0
			y += line_height
		else:
			if case == "upper":
				char = char.upper()
			elif case == "lower":
				char = char.lower()
			char_filename = "{}/fonts/{}/{:04x}.png".format(folder, font, ord(char)) #Get character codepoint in hex and pad it with zeroes
			
			if os.path.exists(char_filename):
				char_image = Image.open(char_filename)
			elif font == "smallWide":
				char_filename = "{}/fonts/small/{:04x}.png".format(folder, ord(char)) #Get character codepoint in hex and pad it with zeroes
				if os.path.exists(char_filename):
					char_image = Image.open(char_filename)
				else:
					char_image = Image.open("{}/fonts/small/tofu.png".format(folder))
			else:
				char_image = Image.open("{}/fonts/{}/tofu.png".format(folder, font))

			pixels = char_image.load()
			colored = char_image#.copy()
			for wai in range(char_image.height):
				for eks in range(char_image.width):
					if pixels[eks, wai] == (255, 255, 255, 255):
						colored.putpixel((eks, wai), color)
			
			image.paste(char_image, (x + x_offset + xPos, y + y_offset + yPos), char_image)
			x += char_image.width
	return image

def textWidth(message, font):
	file_path = os.path.abspath(__file__)
	folder = os.path.dirname(file_path)

	line_height = x_offset = y_offset = 0
	case = None
	with open("{}/fonts/{}/properties".format(folder, font)) as f:
		lines = f.readlines()
		for line in lines:
			l = line.strip("\n").split("=")
			if l[0] == "height":
				line_height = int(l[1])
			elif l[0] == "case":
				case = l[1]
			elif l[0] == "x_offset":
				x_offset = int(l[1])
			elif l[0] == "y_offset":
				y_offset = int(l[1])

	x = 0
	oldX = 0
	for char in message:
		if char == "\n":
			oldX = max(x, oldX)
			x = 0
		else:
			if case == "upper":
				char = char.upper()
			elif case == "lower":
				char = char.lower()
			char_filename = "{}/fonts/{}/{:04x}.png".format(folder, font, ord(char)) #Get character codepoint in hex and pad it with zeroes
			
			if os.path.exists(char_filename):
				char_image = Image.open(char_filename)
			elif font == "smallWide":
				char_filename = "{}/fonts/small/{:04x}.png".format(folder, ord(char)) #Get character codepoint in hex and pad it with zeroes
				if os.path.exists(char_filename):
					char_image = Image.open(char_filename)
				else:
					char_image = Image.open("{}/fonts/small/tofu.png".format(folder))
			else:
				char_image = Image.open("{}/fonts/{}/tofu.png".format(folder, font))
			
			x += char_image.width
	return max(x, oldX)

def find_octant(x1, y1, x2, y2):
	dx = x1 - x2
	dy = y1 - y2
	if dx == 0:
		if dy > 0:
			return 6
		elif dy < 0:
			return 2
		else:
			return None
	angle = math.atan(-dy/dx)
	octant = round(angle/(math.tau/8))
	if dx < 0:
		octant += 4
	octant %= 8
	return octant

def find_octant_unrounded(x1, y1, x2, y2): #Find octant 1 is in from 2's perspective
	dx = x1 - x2
	dy = y1 - y2
	if dx == 0:
		if dy > 0:
			return 6
		elif dy < 0:
			return 2
		else:
			return None
	angle = math.atan(-dy/dx)
	octant = angle/(math.tau/8)
	if dx < 0:
		octant += 4
	octant %= 8
	return octant

#pseudo chess algebraic notation
def coords(pcan):
    x_str = ""
    y_str = ""

    if pcan[0] in string.ascii_letters:
        x_first = True
    else:
        x_first = False

    for char in pcan:
        if char in string.ascii_letters:
            if x_str == "":
                x_str = char.lower()
            else:
                return None
        elif char in string.digits:
            if x_str == "" or x_first:
                y_str += char
            else:
                return None

    x = ord(x_str) - 97
    y = int(y_str) - 1

    return (x, y)

#check if PCAN
def isCoord(pcan):
	#Null String test
	if pcan == "":
		return False
	#Is hypothetical X first?
	if pcan[0] in string.ascii_letters:
		x_first = True
	elif pcan[0] in string.digits:
		x_first = False
	else:
		return False

	#Ultimate test
	phase = 0
	passed = True
	for char in pcan:
		#Transition to next phase
		if x_first and char in string.digits and phase == 0:
			phase = 1
		elif not x_first and char in string.ascii_letters and phase == 0:
			phase = 1
		#Examine each character
		if x_first and char not in string.ascii_letters and phase == 0:
			passed = False
		elif x_first and char not in string.digits and phase == 1:
			passed = False
		elif not x_first and char not in string.digits and phase == 0:
			passed = False
		elif not x_first and char not in string.ascii_letters and phase == 1:
			passed = False
	return passed

def isInt(input):
	try: 
		int(input)
	except ValueError:
		return False
	else:
		return True

def isNumLet(pcan):
	#Null String test
	if pcan == "":
		return False
	#Is hypothetical X first?
	if pcan[0] not in string.digits:
		return False

	#Ultimate test
	phase = 0
	passed = True
	for char in pcan:
		#Transition to next phase
		if char in string.ascii_letters and phase == 0:
			phase = 1
		#Examine each character
		if char not in string.digits and phase == 0:
			passed = False
		elif char not in string.ascii_letters and phase == 1:
			passed = False
	return passed

def isNumP(pcan):
	#Null String test
	if pcan == "":
		return False
	#Is hypothetical X first?
	if pcan[0] not in string.digits:
		return False

	#Ultimate test
	phase = 0
	for char in pcan:
		#Transition to next phase
		if char in ["p", "P"] and phase == 0:
			phase = 1
		elif phase == 1:
			phase = 2
		#Examine each character
		if char not in string.digits and phase == 0:
			return False
		elif char not in ["p", "P"] and phase == 1:
			return False
	if phase == 0:
		return False
	return True
def isNumPNum(pcan):
	#Null String test
	if pcan == "":
		return False
	#Is hypothetical X first?
	if pcan[0] not in string.digits:
		return False

	#Ultimate test
	phase = 0
	for char in pcan:
		#Transition to next phase
		if char in ["p", "P"] and phase == 0:
			phase = 1
		elif phase == 1:
			phase = 2
		#Examine each character
		if char not in string.digits and phase in [0,2]:
			return False
		elif char not in ["p", "P"] and phase == 1:
			return False
	if phase in [0, 1]:
		return False
	return True

def simpPawnLabel(pcan):
	x_str = ""

	if not isNumP(pcan):
		return None

	phase = 0
	for char in pcan:
		#Transition to next phase
		if char in ["p", "P"] and phase == 0:
			phase = 1
		elif phase == 1:
			phase = 2
		#Examine each character
		if char in string.digits and phase == 0:
			x_str += char

	x = int(x_str) - 1

	return x

def advPawnLabel(pcan):
	x_str = ""
	y_str = ""

	if not isNumPNum(pcan):
		return None

	phase = 0
	for char in pcan:
		#Transition to next phase
		if char in ["p", "P"] and phase == 0:
			phase = 1
		elif phase == 1:
			phase = 2
		#Examine each character
		if char in string.digits and phase == 0:
			x_str += char
		elif char in string.digits and phase == 2:
			y_str += char

	x = int(x_str) - 1
	y = int(y_str) - 1

	return (x, y)

def isLetNum(pcan):
	#Null String test
	if pcan == "":
		return False
	#Is hypothetical X first?
	if pcan[0] not in string.ascii_letters:
		return False

	#Ultimate test
	phase = 0
	for char in pcan:
		#Transition to next phase
		if char in string.digits and phase == 0:
			phase = 1
		#Examine each character
		if char not in string.ascii_letters and phase == 0:
			return False
		elif char not in string.digits and phase == 1:
			return False
	if phase == 0:
		return False
	return True

def addItem(inventory, item):
	for i in range(len(inventory)):
		if inventory[i] == None:
			inventory[i] = item
			return inventory
	inventory.append(item)
	return inventory

def help(command, condition = "", preface = ""):
	if preface == "whatare" and command == "people":
		path = "{}/stuff/halp/people.txt".format(folder)
		if os.path.exists(path):
			with open(path, encoding = "utf-8") as f:
				return f.read()
	else:
		path = "{}/help/{}/{}.txt".format(folder, condition, command)
		if os.path.exists(path):
			with open(path, encoding = "utf-8") as f:
				return f.read()
		else:
			path = "{}/help/{}.txt".format(folder, command)
			if os.path.exists(path):
				with open(path, encoding = "utf-8") as f:
					return f.read()
			return None

def helpExists(command):
	path = "{}/help/{}.txt".format(folder, command)
	if os.path.exists(path):
		return True

	subfolders = ["preparation", "whatare"]
	for i in subfolders:
		path = "{}/help/{}/{}.txt".format(folder, i, command)
		if os.path.exists(path):
			return True
		return False

def buy(Id):
	if Id == "a1":
		return smallMace()
	elif Id == "a4":
		return bow()
	elif Id == "c1":
		return healthPotion()
	#newbuyitem
	else:
		return None

#section 1 math (mathbottom mathend)


#section 2 pawns (pawntop pawnstart)

class Sprite:
	def __init__(self, image, x = 0, y = 0, image_x = 0, image_y = 0, battle = None):
		self.image = image
		self.x = x
		self.y = y
		self.image_x = image_x
		self.image_y = image_y
		self.battle = battle

	def draw_on_tile(self, frame, image = None, includeHp = False):
		if image is None:
			image = self.image

		#Top-left of tile
		tile_x = self.x * 32 + (200 - 16 * self.battle.boardSize) 
		tile_y = self.y * 24 + (150 - 12 * self.battle.boardSize)

		#Where the pawn actually stands
		pixel_x = tile_x + self.image_x
		pixel_y = tile_y + self.image_y
		
		#Crop sprite image if either coordinate is negative
		if pixel_x < 0 or pixel_y < 0:
			image = image.crop((max(0, -pixel_x), max(0, -pixel_y), image.width, image.height))
			x = max(0, pixel_x)
			y = max(0, pixel_y)
		else:
			x = pixel_x
			y = pixel_y

		frame.alpha_composite(image, (x, y))

		if includeHp and self.deathState < 3:
			healthbar = Image.new("RGBA", (256, 256), (0, 0, 0, 0))
			redHpLeftToDraw = self.Hp
			hpLeftToDraw = self.maxHp
			hpDrawPencilX = 0
			fullHeart = Image.open("{}/sprites/heart_bigfull.png".format(folder))
			halfHeart = Image.open("{}/sprites/heart_bighalf.png".format(folder))
			while hpLeftToDraw > 0:
				if redHpLeftToDraw >= 2:
					tag = "2"
					redHpLeftToDraw -= 2
				elif redHpLeftToDraw >= 1:
					tag = "1"
					redHpLeftToDraw -= 1
				else:
					tag = "0"
				if redHpLeftToDraw < 1 and redHpLeftToDraw % 1 >= 0.5:
					tag += "h"
					redHpLeftToDraw -= 0.5
				if hpLeftToDraw >= 2:
					tag = "full" + tag
					hpLeftToDraw -= 2
					nextHeart = Image.open("{}/sprites/heart_{}.png".format(folder, tag))
					healthbar.alpha_composite(nextHeart, (hpDrawPencilX, 0))
					hpDrawPencilX += 6
				elif hpLeftToDraw == 1:
					tag = "half" + tag
					hpLeftToDraw -= 1
					nextHeart = Image.open("{}/sprites/heart_{}.png".format(folder, tag))
					healthbar.alpha_composite(nextHeart, (hpDrawPencilX, 0))
					hpDrawPencilX += 4

			if self.Hp > 0 and not self.PhantomState:#!= self.maxHp:
				healthbar = healthbar.crop((0,0, hpDrawPencilX+1,7))

				frame.alpha_composite(healthbar, (x+32-(hpDrawPencilX // 2),y+46))

	def draw_on(self, frame, x, y, image = None):
		if image is None:
			image = self.image

		if x < 0 or y < 0:
			image = image.crop((max(0, -x), max(0, -y), image.width, image.height))
			x = max(0, x)
			y = max(0, y)

		frame.alpha_composite(image, (x, y))
	
class Pawn(Sprite):
	def __init__(self, game, player = None, name = "", x = 0, y = 0, angle = 5, stats = {}):
		self.game = game
		self.player = player
		self.name = name
		self.x = x
		self.y = y
		self.angle = angle
		self.potionAngle = angle 	#Used to reset the position a pawn is facing if it uses certain items
		self.spriteState = "normal"
		self.particleCount = 0
		self.particleType = ""
		self.deathState = 0 #0 = Alive; 1~2 = Dead & still visible; 3 = Dead & invisible
		self.deathCounter = 0
		self.defendState = 0
		self.defendHp = 3	#Bonus DEF that is channeled with the "defend" action. Depleted when attacked in that state.
		self.turnActive = False

		self.image_x = -32 + 16 + random.randrange(-6, 7)
		self.image_y = -43 + 14 + random.randrange(-5, 5)

		self.maxHp = stats.get('maxhp')
		self.Hp = stats.get('hp', self.maxHp)
		self.latestHp = self.Hp
		self.maxMp = stats.get('maxmp')
		self.Mp = stats.get('mp', self.maxMp)
		self.maxMov = 3#stats.get('mov')
		self.Mov = 0 #This variable may change as part of various events in the game

		self.item1 = stats.get('item1')	#Right hand
		self.item2 = stats.get('item2') #Left hand
		self.item3 = stats.get('item3')	#Pocket 1
		self.item4 = stats.get('item4')	#Pocket 2
		self.armor = stats.get('armor')
		self.items = [self.item1, self.item2, self.item3, self.item4]
		self.magic1 = stats.get('magic1')	#Active ability
		self.magic2 = stats.get('magic2')	#Active ability
		self.magic3 = stats.get('magic3')	#Any ability
		self.magic4 = stats.get('magic4')	#Passive ability
		self.magics = [self.magic1, self.magic2, self.magic3, self.magic4]
		self.inventory = [self.item1, self.item2, self.item3, self.item4, self.magic1, self.magic2, self.magic3, self.magic4, self.armor]

		self.stats = stats
		self.PhantomState = True
		self.genesis()
		#stuff related to turn rules
		self.latestWeapon = None
		self.latestWeaponDisp = None
		self.immuneToDamageStunFrom = -1
		self.willbeImmuneToDamageStunFrom = []
		self.wasImmuneToDamageStunFrom = []
		
	def update_sprite(self, isAlly, preview = False):
		if preview:
			if isAlly:
				anglevar = 5
			else:
				anglevar = 7
		else:
			anglevar = self.angle

		if anglevar == None:
			anglevar = 6
			print('anglevar was none error')

		cond = ""

		offsetArms = 0
		offsetArmsX = 0
		offsetArmsY = 0
		offsetLegs = 0
		offsetBody = 0
		offsetHead = 0

		#Central body - Head, Body, Legs
		legs_sheet = Image.open("{}/sprites/pawns/pawn_legs.png".format(folder))
		body_sheet = Image.open("{}/sprites/pawns/pawn_body.png".format(folder))
		head_sheet = Image.open("{}/sprites/pawns/pawn_head.png".format(folder))
		if self.defendState == 1:
			head_sheet = Image.open("{}/sprites/pawns/pawn_head_sleep.png".format(folder))
			dictOffsetsX = [0, -2, -3, -2, 0, 2, 3, 2]
			dictOffsetsY = [2, 1, 0, -1, -2, -1, 0, 1]
			offsetArmsX = dictOffsetsX[anglevar]
			offsetArmsY = dictOffsetsY[anglevar]
			offsetLegs = 0
			offsetArms = -1
			offsetBody = -3
			offsetHead = -5
		else:
			if self.spriteState == "hurt":
				legs_sheet = Image.open("{}/sprites/pawns/pawn_legs_hurt.png".format(folder))
				head_sheet = Image.open("{}/sprites/pawns/pawn_head_hurt.png".format(folder))
			if self.spriteState == "fight":
				head_sheet = Image.open("{}/sprites/pawns/pawn_head_fight.png".format(folder))
				cond = "_swing"
			if self.spriteState == "mad":
				head_sheet = Image.open("{}/sprites/pawns/pawn_head_fight.png".format(folder))
			if self.spriteState == "shock":
				head_sheet = Image.open("{}/sprites/pawns/pawn_head_shock.png".format(folder))
				cond = "_swing"

		#Arms
		if anglevar in [2, 3, 4, 5]:
			if self.item1 != None and self.item1 == self.latestWeapon:
				backarm_sheet = self.item1.imageGet(1, cond)	#Right
			elif self.item1 != None:
				backarm_sheet = self.item1.imageGet(1)
			else:
				backarm_sheet = Image.open("{}/sprites/pawns/pawn_arm_right.png".format(folder))
			if self.item2 != None and self.item2 == self.latestWeapon:	#Left
				frontarm_sheet = self.item2.imageGet(2, cond)
			elif self.item2 != None:
				frontarm_sheet = self.item2.imageGet(2)
			else:
				frontarm_sheet = Image.open("{}/sprites/pawns/pawn_arm_left.png".format(folder))
			offsetArmsX *= -1
			offsetArmsY *= -1
		if anglevar in [0, 1, 6, 7]:
			if self.item1 != None and self.item1 == self.latestWeapon:	#Right
				frontarm_sheet = self.item1.imageGet(1, cond)
			elif self.item1 != None:
				frontarm_sheet = self.item1.imageGet(1)
			else:
				frontarm_sheet = Image.open("{}/sprites/pawns/pawn_arm_right.png".format(folder))
			if self.item2 != None and self.item2 == self.latestWeapon:	#Left
				backarm_sheet = self.item2.imageGet(2, cond)
			elif self.item2 != None:
				backarm_sheet = self.item2.imageGet(2)
			else:
				backarm_sheet = Image.open("{}/sprites/pawns/pawn_arm_left.png".format(folder))

		#Accessories
		acBack2 = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
		acBack1 = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
		acFront1 = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
		acFront2 = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
		if self.item3 != None:
			ac3Sheet = self.item3.imageGet(3)
		else:
			ac3Sheet = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
		if self.item4 != None:
			ac4Sheet = self.item4.imageGet(4)
		else:
			ac4Sheet = Image.new("RGBA", (512, 64), (0, 0, 0, 0))

		if anglevar == 0:
			acFront2.paste(ac3Sheet, (0,0), ac3Sheet)
			acBack2.paste(ac4Sheet, (0,0), ac4Sheet)
		elif anglevar == 1:						#States of accessories in each frame:
			acFront2.paste(ac3Sheet, (0,0), ac3Sheet)	#    Front|Back
			acBack1.paste(ac4Sheet, (0,0), ac4Sheet)	#     2 1 | 1 2
		elif anglevar == 2:								#   (14|13|12|11)
			acFront1.paste(ac3Sheet, (0,0), ac3Sheet)	#         |
			acFront1.paste(ac4Sheet, (0,0), ac4Sheet)	#0:   3   |   4
		elif anglevar == 3:								#1:   3   | 4
			acBack1.paste(ac3Sheet, (0,0), ac3Sheet)	#2:     34|
			acFront2.paste(ac4Sheet, (0,0), ac4Sheet)	#3:   4   | 3
		elif anglevar == 4:								#4:   4   |   3
			acBack2.paste(ac3Sheet, (0,0), ac3Sheet)	#5:     4 |   3
			acFront2.paste(ac4Sheet, (0,0), ac4Sheet)	#6:       |34  
		elif anglevar == 5:								#7:     3 |   4
			acBack2.paste(ac3Sheet, (0,0), ac3Sheet)
			acFront1.paste(ac4Sheet, (0,0), ac4Sheet)
		elif anglevar == 6:
			acBack1.paste(ac3Sheet, (0,0), ac3Sheet)
			acBack1.paste(ac4Sheet, (0,0), ac4Sheet)
		elif anglevar == 7:
			acFront1.paste(ac3Sheet, (0,0), ac3Sheet)
			acBack2.paste(ac4Sheet, (0,0), ac4Sheet)

		sheet_x1 = anglevar * 64
		sheet_y1 = 0
		sheet_x2 = (anglevar + 1) * 64
		sheet_y2 = 64
		crop_coords = (sheet_x1, sheet_y1, sheet_x2, sheet_y2)

		backarm = backarm_sheet.crop(crop_coords)
		acBack2 = acBack2.crop(crop_coords)
		acBack1 = acBack1.crop(crop_coords)
		legs = legs_sheet.crop(crop_coords)
		body = body_sheet.crop(crop_coords)
		head = head_sheet.crop(crop_coords)
		acFront1 = acFront1.crop(crop_coords)
		acFront2 = acFront2.crop(crop_coords)
		frontarm = frontarm_sheet.crop(crop_coords)

		image = Image.new("RGBA", (64, 64))

		hurtOffsetsX = [-2, -2, 0, 2, 2, 2, 0, -2]
		hurtOffsetsY = [0, 1, 1, 1, 0, -1, -1, -1]
		if self.spriteState == "hurt" and self.defendState != 1:
			eks = hurtOffsetsX[anglevar]
			wai = hurtOffsetsY[anglevar]
		else:
			eks = 0
			wai = 0

		if anglevar != 6:
			image.paste(backarm, (eks - offsetArmsX, wai + offsetArmsY - offsetArms), backarm)
		if anglevar == 2:
			image.paste(frontarm, (eks + offsetArmsX, wai - offsetArmsY - offsetArms), frontarm)
		image.paste(acBack2, (eks, wai - offsetBody), acBack2)
		image.paste(acBack1, (eks, wai - offsetBody), acBack1)
		image.paste(legs, (-eks, -wai - offsetLegs), legs)
		image.paste(body, (eks, wai - offsetBody), body)
		image.paste(head, (eks, wai - offsetHead), head)
		image.paste(acFront1, (eks, wai - offsetBody), acFront1)
		image.paste(acFront2, (eks, wai - offsetBody), acFront2)
		if anglevar != 2:
			image.paste(frontarm, (eks + offsetArmsX, wai - offsetArmsY - offsetArms), frontarm)
		if anglevar == 6:
			image.paste(backarm, (eks - offsetArmsX, wai + offsetArmsY - offsetArms), backarm)

		if (isAlly and self.PhantomState) or self.deathState in [1,2]:
			alphaval = 127	#Constant
		elif (not isAlly and self.PhantomState) or self.deathState == 3:
			alphaval = 0

		if (self.PhantomState and not preview) or self.deathState >= 1:
			imageBands = image.split()
			imageBands[3].paste(imageBands[3].point(lambda i: min(i, alphaval)))
			image = Image.merge("RGBA", imageBands)
		elif self.deathState == 0:
			alphaval = 255
		
		self.image = image
		self.colored_images = self.color_sprite(image, alphaval)

		#add forcefield
		if self.defendState == 1 and not preview:
			forcefield = Image.open("{}/sprites/shieldforcefield.png".format(folder))
			for i in self.colored_images.values():
				i.paste(forcefield, (22, 22), forcefield)

		#add damage particles
		if not preview and self.particleType != "": # self.spriteState == "hurt" and not preview:
			c = self.particleCount
			while c > 0:
				p = random.random() * c
				if p >= 32:
					particleSize = "huge"
					c -= 36
					eks = random.randrange(12) + 26
					wai = random.randrange(12) + 28
				elif p >= 21:
					particleSize = "large"
					c -= 25
					eks = random.randrange(14) + 25
					wai = random.randrange(14) + 27
				elif p >= 12:
					particleSize = "medium"
					c -= 16
					eks = random.randrange(16) + 24
					wai = random.randrange(16) + 26
				elif p >= 5:
					particleSize = "small"
					c -= 9
					eks = random.randrange(18) + 23
					wai = random.randrange(18) + 25
				else:
					particleSize = "tiny"
					c -= 4
					eks = random.randrange(20) + 22
					wai = random.randrange(20) + 24
			
				filename = "{}/sprites/particles/{}/{}.png".format(folder, self.particleType, particleSize)
				if os.path.exists(filename):
					particle = Image.open(filename)
					for i in self.colored_images.values():
						i.paste(particle, (eks, wai), particle)
				else:
					foldername = "{}/sprites/particles/{}".format(folder, self.particleType)
					if os.path.exists(foldername):
						#calculate number of variants
						variants = 1
						continuing = True
						while continuing:
							filename = "{}/sprites/particles/{}/{}.png".format(folder, self.particleType, particleSize + str(variants))
							if os.path.exists(filename):
								variants += 1
							else:
								continuing = False
						#add random variation
						selection = random.randrange(variants) + 1
						filename = "{}/sprites/particles/{}/{}.png".format(folder, self.particleType, particleSize + str(selection))
						particle = Image.open(filename)
						for i in self.colored_images.values():
							i.paste(particle, (eks, wai), particle)

	def color_sprite(self, image, alphaval = 127):
		colors = {
			"white": [(255, 255, 255, 255), (204, 204, 204, 255), (153, 153, 153, 255)],
			"black": [(102, 102, 102, 255), (51, 51, 51, 255), (0, 0, 0, 255)],
			"red": [(255, 29, 44, 255), (191, 22, 33, 255), (127, 14, 22, 255)],
			"blue": [(0, 182, 255, 255), (0, 137, 191, 255), (0, 91, 127, 255)],
			"yellow": [(255, 211, 0, 255), (191, 158, 0, 255), (127, 105, 0, 255)],
			"green": [(0, 204, 68, 255), (0, 153, 51, 255), (0, 102, 34, 255)],
			"purple": [(95, 0, 191, 255), (71, 0, 143, 255), (47, 0, 95, 255)],
			"orange": [(255, 120, 22, 255), (191, 90, 17, 255), (127, 60, 11, 255)]
		}
		colorsTransp = {
			"white": [(255, 255, 255, alphaval), (204, 204, 204, alphaval), (153, 153, 153, alphaval)],
			"black": [(102, 102, 102, alphaval), (51, 51, 51, alphaval), (0, 0, 0, alphaval)],
			"red": [(255, 29, 44, alphaval), (191, 22, 33, alphaval), (127, 14, 22, alphaval)],
			"blue": [(0, 182, 255, alphaval), (0, 137, 191, alphaval), (0, 91, 127, alphaval)],
			"yellow": [(255, 211, 0, alphaval), (191, 158, 0, alphaval), (127, 105, 0, alphaval)],
			"green": [(0, 204, 68, alphaval), (0, 153, 51, alphaval), (0, 102, 34, alphaval)],
			"purple": [(95, 0, 191, alphaval), (71, 0, 143, alphaval), (47, 0, 95, alphaval)],
			"orange": [(255, 120, 22, alphaval), (191, 90, 17, alphaval), (127, 60, 11, alphaval)]
		}
		sprites = {}
		pixels = image.load()
		for color in colors:
			colored = image.copy()
			for y in range(image.height):
				for x in range(image.width):
					if pixels[x, y] == (0, 255, 0, 255): #Light
						colored.putpixel((x, y), colors[color][0])
					elif pixels[x, y] == (0, 166, 0, 255): #Medium
						colored.putpixel((x, y), colors[color][1])
					elif pixels[x, y] == (25, 123, 48, 255): #Dark
						colored.putpixel((x, y), colors[color][2])
					elif pixels[x, y] == (0, 255, 0, alphaval): #Light
						colored.putpixel((x, y), colorsTransp[color][0])
					elif pixels[x, y] == (0, 166, 0, alphaval): #Medium
						colored.putpixel((x, y), colorsTransp[color][1])
					elif pixels[x, y] == (25, 123, 48, alphaval): #Dark
						colored.putpixel((x, y), colorsTransp[color][2])
			sprites[color] = colored

		return sprites
	
	def genesis(self):

		yeetHp = 5 + random.randrange(0,2)
		self.maxHp = yeetHp
		self.Hp = yeetHp		

	def pathfind(self): #arrCol is an array describing the tactical view of the environment
		#possible arrCol values = "clear", "barr", "gap", "wall"...

		boardSize = self.battle.boardSize
		arrCol = self.battle.arrCol.copy()
		pawnCol = self.battle.pawnCol.copy()
		if self.PhantomState:
			self.Mov = boardsizeCal_unrounded(len(self.player.pawns)) // 2

		arrDist = []    #Contains information about pythagorean distance 
		for i in range(0, boardSize):
			arrDist.append([-0.5] * boardSize)
		arrDist[self.x][self.y] = self.Mov

		arrGotherefrom = [] #Identifies what tile a pawn must have come from to get here
		for i in range(0, boardSize):
			arrGotherefrom.append([(self.x,self.y)] * boardSize)

		arrNew = [] #Used to check for changes to the pathfinding
		for i in range(0, boardSize):
			arrNew.append([0] * boardSize)
		arrNew[self.x][self.y] = 1

		arrNew1 = []    #Used to check for changes to the pathfinding
		for i in range(0, boardSize):
			arrNew1.append([0] * boardSize)
		arrNew1[self.x][self.y] = 1

		#possible arrMov values = "you", "mact", "move", "far", "blocked"
		arrMov = []
		for i in range(0, boardSize):
			arrMov.append([""] * boardSize)

		#Fill out arrDist
		updated = True
		while updated:
			updated = False

			#Step 1: Find a tile in arrNew with value 1
			for wai0 in range(0, boardSize):
				for eks0 in range(0, boardSize):
					if arrNew[eks0][wai0] == 1: #We have found a tile to pathfind from!

						#Step 2: Identify all the tiles we can get to by moving in a straight line
						rangeUno = arrDist[eks0][wai0]
						Range = math.ceil(rangeUno)
						searchLeft = max(0, eks0 - Range)
						searchRight = min(boardSize, eks0 + Range + 1)
						searchTop = max(0, wai0 - Range)
						searchBottom = min(boardSize, wai0 + Range + 1)
						for wai1 in range(searchTop, searchBottom):
							for eks1 in range(searchLeft, searchRight):
								if arrNew[eks1][wai1] == 0:

									#Step 3: Check if all the tiles between here and there are empty
									isPathClear = True

									for wai2 in range(min(wai0,wai1), max(wai0,wai1) + 1):
										for eks2 in range(min(eks0,eks1), max(eks0,eks1) + 1):
											if arrCol[eks2][wai2] != "clear":# or pawnCol[eks2][wai2] != "clear": #in future: check if a pawn is part of the same team
												isPathClear = False
											elif pawnCol[eks2][wai2] != None and not ((eks2, wai2) == (self.x, self.y) and self.PhantomState):
												if pawnCol[eks2][wai2].player != self.player:
													isPathClear = False

									#Step 4: Evaluate the result
									if isPathClear:
										yeetGone = rangeUno - max(abs(eks0 - eks1), abs(wai0 - wai1)) #(((eks0 - eks1) ** 2) + ((wai0 - wai1) ** 2)) ** (1/2)
										yeetHere = arrDist[eks1][wai1]
										if yeetGone > yeetHere: #If the pawn would be able to get to this tile given its Movement value
											arrDist[eks1][wai1] = yeetGone
											arrGotherefrom[eks1][wai1] = (eks0,wai0)
											arrNew1[eks1][wai1] = 1
											updated = True
											#Goddamn, that's a lot of conditionals.

						#Step 5: Mark the tile as having been calculated for
						arrNew1[eks0][wai0] = 2

			#Check if there are any 1-tiles in arrNew1 which are completely surrounded by 1-tiles and 2-tiles (Optional, but it should reduce calculation times)

			#Step 6: Update arrNew
			for wai3 in range(0, boardSize):
				for eks3 in range(0, boardSize):
					arrNew[eks3][wai3] = arrNew1[eks3][wai3]

		#Step 7: Fill out arrMov
		for wai in range(0, boardSize):
			for eks in range(0, boardSize):
				if eks == self.x and wai == self.y:
					arrMov[eks][wai] = "you"
				elif arrCol[eks][wai] == "clear":
					if pawnCol[eks][wai] == None:
						if arrDist[eks][wai] > 0.5:
							arrMov[eks][wai] = "mact"
						elif arrDist[eks][wai] > -0.5:
							arrMov[eks][wai] = "move"
						else:
							arrMov[eks][wai] = "far"
					else:
						arrMov[eks][wai] = "pawnblocked"
				else:
					arrMov[eks][wai] = "blocked"
		
		self.arrDist = arrDist
		self.arrOrigin = arrGotherefrom
		self.arrMov = arrMov

	def targetfind(self, weapon = None):

		boardSize = self.battle.boardSize
		arrCol = self.battle.arrCol.copy()
		pawnCol = self.battle.pawnCol.copy()

		arrTarg = []
		for i in range(0, boardSize):
			arrTarg.append([""] * boardSize)

		if weapon == None:
			for i in self.inventory:
				if i != None:
					if i.type == "weapon":
						found = i.findenemy()

						for wai in range(len(found)):
							for eks in range(len(found[wai])):
								if found[eks][wai] == "enemy":
									#self.arrMov[eks][wai] = "enemy"
									arrTarg[eks][wai] = "enemy"
								elif found[eks][wai] in ["ally","inrange"]:
									arrTarg[eks][wai] = "inrange"
		else:
			if weapon in self.inventory:
				if weapon.type == "weapon":
					found = weapon.findenemy()

					for wai in range(len(found)):
						for eks in range(len(found[wai])):
							if found[eks][wai] == "enemy":
								#self.arrMov[eks][wai] = "enemy"
								arrTarg[eks][wai] = "enemy"
							elif found[eks][wai] in ["ally","inrange"]:
								arrTarg[eks][wai] = "inrange"

		self.arrTarg = arrTarg

	def move(self, x, y):
		self.pathfind()
		if self.arrMov[x][y] in ["mact", "move"]:
			if self.arrMov[x][y] == "move":
				self.Mov = self.maxMov + self.arrDist[x][y] #This singular line should (hopefully) make a pawn's movement options more circular even beyond their normal range
			elif self.arrMov[x][y] == "mact":
				self.Mov = self.arrDist[x][y]
			if self.PhantomState:
				self.PhantomState = False
			else:
				self.battle.pawnCol[self.x][self.y] = None
			self.undef()
			self.angle = find_octant(x, y, self.arrOrigin[x][y][0], self.arrOrigin[x][y][1])
			self.potionAngle = self.angle
			self.x = x
			self.y = y
			self.image_x = -32 + 16 + random.randrange(-2, 3)
			self.image_y = -43 + 14 + random.randrange(-2, 2)
			self.battle.pawnCol[self.x][self.y] = self
		return self.arrMov[x][y]

	def wait(self):#, advanceShield = True):
		if self.PhantomState:
			self.Mov = boardsizeCal_unrounded(len(self.player.pawns)) / 2
		else:
			self.Mov = self.maxMov

		#if advanceShield:
		#	if self.defendState == 1:
		#		self.defendState = 2
		#	elif self.defendState == 2:
		#		self.defendState = 0
		#		self.defendHp = 150

		for i in range(len(self.inventory)):
			if self.inventory[i] != None:
				if self.inventory[i].consume():
					self.take(i + 1)

	def turn(self, x, y):
		if x != self.x or y != self.y:
			result = self.undef()
			if result == "success":
				self.angle = find_octant(x, y, self.x, self.y)
			return result
		else:
			return "no"
	def turnDir(self, x):
		dictDirs = {"north": 2, "n": 2, "northeast": 1, "ne": 1, "east": 0, "e": 0, "southeast": 7, "se": 7, "south": 6, "s": 6, "southwest": 5, "sw": 5, "west": 4, "w": 4, "northwest": 3, "nw": 3}
		result = self.undef()
		if result == "success":
			self.angle = dictDirs[x]
		return result

	async def doDamage(self, targetX, targetY, weapon):
		pawnCol = self.battle.pawnCol.copy()
		target = pawnCol[targetX][targetY]
		self.turn(targetX, targetY)
		if self.PhantomState:
			self.PhantomState = False
			self.battle.pawnCol[self.x][self.y] = self

		Atk = weapon.Atk

		angle = find_octant_unrounded(targetX, targetY, self.x, self.y)

		if target != None:
			result = await target.takeDamage(Atk, weapon, angle, self)
		else:
			result = 1
		if result <= 0:
			self.spriteState = "shock-0"
		else:
			self.spriteState = "fight-0"
		self.latestWeapon = weapon	#Attack Stun
		self.latestWeaponDisp = weapon 	#Display
		self.wait()

	async def takeDamage(self, Atk, weapon, angle, attacker):
		Def = 0

		#Calculate flanking bonus
		angDiff = abs(4 - abs(angle - self.angle))
		if self.defendState == 1:
			angDiff = 0
			Def += 2
		flank = max(angDiff - 2, 0) * 2

		Damage = (Atk + flank) - Def
		self.Hp -= max(0, Damage)

		if self.defendState == 1:
			self.defendHp -= max(0, Damage + Def)

		#yeetMax = self.maxMov
		#if Damage > 0 and attacker.Id not in [self.immuneToDamageStunFrom] + self.wasImmuneToDamageStunFrom:
		#	yeetMax = self.maxMov - 1	#Damage Stun
		#	self.willbeImmuneToDamageStunFrom.append(attacker.Id)
		#self.Mov = min(yeetMax, self.Mov)

		#Step 3: If the enemy's HP <= 0, remove them from the board
		if self.Hp <= 0:
			#self.battle.pawns.remove(self)
			self.deathState = 1
			self.deathCounter += 1
			self.defendState = 0
			self.battle.pawnCol[self.x][self.y] = None

			predicates = ["has fallen!", "was defeated!", "was killed!", "has died!"]
			msg = "{} ({}'s army) {}".format(self.name, self.player.member.name, random.choice(predicates))
			for team in self.battle.teams:
				await team.channel.channel.send(msg)
			for lobby in self.battle.lobbies:
				await lobby.send(msg)

		if Damage > 0:
			self.spriteState = "hurt-0"
		else:
			self.spriteState = "mad-0"
		self.particleType = "normal-0"
		self.particleCount = Damage * 10
		return Damage

	async def attack(self, targetX, targetY, weapon = "ngeng"):
		pawnCol = self.battle.pawnCol.copy()
		#Step 1: Identify an enemy
		if not (targetX == self.x and targetY == self.y):# and pawnCol[targetX][targetY] != None:
			if weapon == "ngeng":
				for i in range(len(self.inventory)):
					item = self.inventory[i]
					if item != None:
						if item.type == "weapon" and ((i in [0,1] and item.needsHeld) or not item.needsHeld):
							if item.findenemy(False)[targetX][targetY] in ["enemy", "ally", "inrange"]: #if pawnCol[targetX][targetY] != None and ... == "enemy"
								result = self.undef()
								if result == "success":
									#Step 2: Attack the target
									await self.doDamage(targetX, targetY, item)
								return result
							else:
								return "outofrange"
				return "noweapons"
			else:
				if weapon != None:
					if weapon.type == "weapon":
						if (i in [self.inventory[0], self.inventory[1]] and weapon.needsHeld) or not weapon.needsHeld:
							if weapon.findenemy(False)[targetX][targetY] in ["enemy", "ally", "inrange"]: #if pawnCol[targetX][targetY] != None and ... == "enemy"
								result = self.undef()
								if result == "success":
									#Step 2: Attack the target
									await self.doDamage(targetX, targetY, weapon)
								return result
							else:
								return "outofrange"
						else:
							return "needsheld"
					else:
						return "notaweapon"
				else:
					return "notanitem"
		else:
			return "nosuicide"

	def use(self, item, target = None):
		itemI = int(item) - 1
		if target != None:
			target = coords(target)

		if self.inventory[itemI] == None:
			return "noitem"
		elif self.inventory[itemI].needsHeld and itemI not in [0,1]:
			return "needsheld"
		else:
			result = self.undef()
			if result == "success":
				result = self.inventory[itemI].use(target)
				self.particleType = self.inventory[itemI].relay_particleType + "-0"
				self.particleCount = self.inventory[itemI].relay_particleCount
				if result == "weapon":
					result = self.attack(target[0], target[1], self.inventory[itemI])
				if result == "successturn":
					if self.Mov <= 1:
						self.angle = self.potionAngle
					result = "successend"
				if result == "successend":
					result = "success"
					self.wait()
			return result

	def defend(self):
		self.defendState = 1
		if self.Mov <= 1:
			self.angle = self.potionAngle
		self.wait()

	def undef(self):
		if self.battle.pawnCol[self.x][self.y] != None and self.PhantomState:
			return "phantomstate"
		else:
			if self.PhantomState:
				self.PhantomState = False
				self.battle.pawnCol[self.x][self.y] = self
			self.spriteState = "normal"
			self.defendState = 0
			return "success"

	def give(self, item, slot): #`slot` must be between 1~9
		if item != None:
			item.user = self
		if slot in range(1, 5):
			self.items[slot - 1] = item
		if slot in range(5, 9):
			self.magics[slot - 5] = item
		self.inventory[slot - 1] = item
		#dictSlots = {1:self.item1, 2:self.item2, 3:self.item3, 4:self.item4, 5:self.magic1, 6:self.magic2, 7:self.magic3, 8:self.magic4, 9:self.armor}
		if slot == 1:
			self.item1 = item
		elif slot == 2:
			self.item2 = item
		elif slot == 3:
			self.item3 = item
		elif slot == 4:
			self.item4 = item
		elif slot == 5:
			self.magic1 = item
		elif slot == 6:
			self.magic2 = item
		elif slot == 7:
			self.magic3 = item
		elif slot == 8:
			self.magic4 = item
		elif slot == 9:
			self.armor = item

	def take(self, slot): #`slot` must be between 1~9
		item = None
		if slot in range(1, 5):
			self.items[slot - 1] = None
		if slot in range(5, 9):
			self.magics[slot - 5] = None
		self.inventory[slot - 1] = None
		#dictSlots = {1:self.item1, 2:self.item2, 3:self.item3, 4:self.item4, 5:self.magic1, 6:self.magic2, 7:self.magic3, 8:self.magic4, 9:self.armor}
		if slot == 1:
			item = self.item1
			self.item1 = None
		elif slot == 2:
			item = self.item2
			self.item2 = None
		elif slot == 3:
			item = self.item3
			self.item3 = None
		elif slot == 4:
			item = self.item4
			self.item4 = None
		elif slot == 5:
			item = self.magic1
			self.magic1 = None
		elif slot == 6:
			item = self.magic2
			self.magic2 = None
		elif slot == 7:
			item = self.magic3
			self.magic3 = None
		elif slot == 8:
			item = self.magic4
			self.magic4 = None
		elif slot == 9:
			item = self.armor
			self.armor = None

		if item != None:
			item.user = None
		return item

	def checkItem(self, slot): #`slot` must be between 1~9
		if slot == 1:
			return self.item1
		elif slot == 2:
			return self.item2
		elif slot == 3:
			return self.item3
		elif slot == 4:
			return self.item4
		elif slot == 5:
			return self.magic1
		elif slot == 6:
			return self.magic2
		elif slot == 7:
			return self.magic3
		elif slot == 8:
			return self.magic4
		elif slot == 9:
			return self.armor

	def putIn(self, battle):
		self.battle = battle
		self.turnActive = False

	#Retire these later!
	def calAtk(self, displayMode = False):
		if self.game.redFog:
			Atk = self.Atk * (self.Hp / self.maxHp)
		else:
			Atk = self.Atk
		if displayMode and self.latestWeaponDisp != None:
			Atk += self.latestWeaponDisp.Atk
		return Atk

	def calDef(self):#, displayMode = False):
		if self.game.redFog:
			Def = self.Def * (self.Hp / self.maxHp)
		else:
			Def = self.Def
		if self.defendState == 1:
			Def = Def + self.defendHp
		return Def

	def calSpd(self):
		spdVal = self.Spd + 100
		if self.game.redFog:
			spdVal = spdVal * (self.Hp / self.maxHp * 0.5 + 0.5)
		if self.latestWeapon != None:
			spdVal -= self.latestWeapon.Wgt
		if self.defendState == 1:
			spdVal /= 2
		return spdVal

	def calMag(self):
		return self.Mag

	def calMov(self):
		if self.PhantomState:
			self.Mov = boardsizeCal_unrounded(len(self.player.pawns)) // 2
		movVal = self.Mov * 50
		return movVal

	def respawn(self):
		#Step 1: Check if able to respawn
		canRespawn = True
		if self.respawns <= 0:
			canRespawn = False
		if self.player.respawns <= 0:
			canRespawn = False
		if self.player.team.respawns <= 0:
			canRespawn = False
		#Step 2: Identify respawn location
		if canRespawn:
			self.respawns -= 1
			self.player.respawns -= 1
			self.player.team.respawns -= 1
			#Calculate distance between starting point and current location
			dist = distance((self.x, self.y), (self.oriX, self.oriY))
			ratio = max([0, 1 - 0.5 ** ((dist-3)/math.sqrt(3))])
			newX = self.x*ratio + self.oriX*(1-ratio)
			newY = self.y*ratio + self.oriY*(1-ratio)
			arrCol = self.battle.arrCol.copy()
			pawnCol = self.battle.pawnCol.copy()
			closest = None
			candidates = []
			for wai in range(self.battle.boardSize):
				for eks in range(self.battle.boardSize):
					if arrCol[eks][wai] == "clear" and pawnCol[eks][wai] == None:
						current = distance((newX,newY),(eks,wai))
						if closest == None:
							closest = current
							candidates.append((eks,wai))
						elif current < closest:
							closest = current
							candidates = []
							candidates.append((eks,wai))
						elif current == closest:
							candidates.append((eks,wai))
			selection = random.choice(candidates)
			#Step 3: Identify facing angle
			enemies = []	#3a: Gather list of enemies
			for p in self.battle.pawns:
				if p.player.team != self.player.team and not (p.PhantomState or p.deathState > 0):
					enemies.append(p)
			enemyAvgX = 0	#3b: Find average of enemies' coordinates
			enemyAvgY = 0
			for p in enemies:
				enemyAvgX += p.x
				enemyAvgY += p.y
			if len(enemies) != 0:
				enemyAvgX /= len(enemies)
				enemyAvgY /= len(enemies)
			else:	#Prevent divisions by 0
				enemyAvgX = (self.battle.boardSize + 1) / 2
				enemyAvgY = (self.battle.boardSize + 1) / 2
			#Step 4: Finalize respawn
			self.x = selection[0]
			self.y = selection[1]
			self.angle = find_octant(enemyAvgX - 1, enemyAvgY - 1, self.x, self.y)
			self.PhantomState = True
			self.deathState = 0
			self.chance = 1 - (self.Hp / 100)
			self.Hp = self.maxHp
		return canRespawn

#section 2 pawns (pawnbottom pawnend)


#section 3 items (itemtop)

class Item():
	def metaInit(self, user, stats, handedness = 1, metatype = None, Type = None, subtype = None, needsHeld = False):
		self.user = user
		self.handedness = handedness
		self.stats = stats
		self.metatype = metatype
		self.type = Type
		self.subtype = subtype
		self.needsHeld = needsHeld

		self.relay_particleType = ""	#Used to deliver information about particle effects
		self.relay_particleCount = 0

	def use(self, target):
		return self.type

	def consume(self):
		return False

	def generateSpritesheet(self, image, frame):
		if frame == 1:	#Right arm
			canvas = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
			#Layer 1: Behind arm
			canvas.paste(image, (81,11), image)		#1
			canvas.paste(image, (147,8), image)		#2
			canvas.paste(image, (204,5), image)		#3
			canvas.paste(image, (265,5), image)		#4

			#Layer 2: The arm itself, and in front of arm
			sheet = Image.open("{}/sprites/pawns/pawn_arm_right.png".format(folder))
			canvas.paste(sheet, (0,0), sheet)
			canvas.paste(image, (11,14), image)		#0
			canvas.paste(image, (320,8), image)		#5
			canvas.paste(image, (382,12), image)	#6
			canvas.paste(image, (457,14), image)	#7

			return canvas

		elif frame == 2:	#Left arm
			canvas = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
			#Layer 1: Behind arm
			canvas.paste(image, (11,5), image)		#0
			canvas.paste(image, (69,5), image)		#1
			canvas.paste(image, (126,8), image)		#2
			canvas.paste(image, (192,11), image)	#3

			#Layer 2: The arm itself, and in front of arm
			sheet = Image.open("{}/sprites/pawns/pawn_arm_left.png".format(folder))
			canvas.paste(sheet, (0,0), sheet)
			canvas.paste(image, (262,14), image)	#4
			canvas.paste(image, (332,14), image)	#5
			canvas.paste(image, (403,12), image)	#6
			canvas.paste(image, (465,8), image)		#7

			return canvas

		elif frame == 3:	#Right pocket
			canvas = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
			#Layer 1: Behind body
			canvas.paste(image, (205,10), image)	#3
			canvas.paste(image, (267,11), image)	#4
			canvas.paste(image, (327,11), image)	#5
			canvas.paste(image, (388,12), image)	#6

			#Layer 2: In front of body
			canvas.paste(image, (6,14), image)		#0
			canvas.paste(image, (75,14), image)		#1
			canvas.paste(image, (141,13), image)	#2
			canvas.paste(image, (453,13), image)	#7

			return canvas

		elif frame == 4:	#Left pocket
			canvas = Image.new("RGBA", (512, 64), (0, 0, 0, 0))
			#Layer 1: Behind body
			canvas.paste(image, (6,11), image)		#0
			canvas.paste(image, (68,10), image)		#1
			canvas.paste(image, (397,12), image)	#6
			canvas.paste(image, (458,11), image)	#7

			#Layer 2: In front of body
			canvas.paste(image, (132,13), image)	#2
			canvas.paste(image, (198,14), image)	#3
			canvas.paste(image, (267,14), image)	#4
			canvas.paste(image, (332,13), image)	#5

			return canvas



class Bludgeon(Item):
	def subInit(self, user, stats, handedness = 1):
		self.metaInit(user, stats, handedness, "item", "weapon", "bludgeon", True)

		self.Atk = self.stats.get("Atk")
		self.Prc = self.stats.get("Prc")
		self.Wgt = self.stats.get("Wgt")

	def findenemy(self, filterAllies = True):
		arrCol = self.user.battle.arrCol.copy()
		lineCol = self.user.battle.lineCol.copy()
		pawnCol = self.user.battle.pawnCol.copy()

		boardSize = len(arrCol)
		arrHit = [[None for x in range(boardSize)] for y in range(boardSize)]
		for wai in range(boardSize):
			for eks in range(boardSize):
				if gridDist([self.user.x, self.user.y], [eks, wai]) == 1 and clearline(arrCol, lineCol, [self.user.x, self.user.y], [eks, wai]) and octogonal(self.user.x - eks, self.user.y - wai):
					if pawnCol[eks][wai] != None:
						if (pawnCol[eks][wai].player.team != self.user.player.team) or not filterAllies:
							arrHit[eks][wai] = "enemy"
						else:
							arrHit[eks][wai] = "ally"
					else:
						arrHit[eks][wai] = "inrange"
		return arrHit

	def drawStats(self):
		image = Image.new("RGBA", (23, 23), (0, 0, 0, 0))	#Black but invisible

		if round(self.Atk) >= 100:
			Ox = 0
		else:
			Ox = 2
		stat1 = Image.open("{}/sprites/iconst_stratk.png".format(folder))
		image.paste(stat1, (0 +Ox, 4), stat1)
		image = drawText(image, str(round(self.Atk)), "small", 8 +Ox, 5)

		if round(self.Prc) >= 100:
			Ox = 0
		else:
			Ox = 2
		stat2 = Image.open("{}/sprites/iconst_prc.png".format(folder))
		image.paste(stat2, (0 +Ox, 12), stat2)
		image = drawText(image, str(round(self.Prc)), "small", 8 +Ox, 13)

		#if round(self.Wgt) >= 100:
		#	Ox = 0
		#else:
		#	Ox = 2
		#stat3 = Image.open("{}/sprites/iconst_wgt.png".format(folder))
		#image.paste(stat3, (0 +Ox, 16), stat3)
		#image = drawText(image, str(round(self.Wgt)), "small", 8 +Ox, 17)

		return image


class smallMace(Bludgeon):
	def __init__(self, user = None, stats = {}):
		self.subInit(user, stats, 1)
		self.Atk = 2

	def imageGet(self, frame, cond = ""):
		if frame == 0:
			return Image.open("{}/sprites/icon_mace.png".format(folder))
		elif frame == 1:
			return Image.open("{}/sprites/pawns/smallMace_right{}.png".format(folder, cond))
		elif frame == 2:
			return Image.open("{}/sprites/pawns/smallMace_left{}.png".format(folder, cond))
		elif frame == 3:
			image = Image.open("{}/sprites/pawns/smallMace_accessory{}.png".format(folder, cond))
			return self.generateSpritesheet(image, 3)
		elif frame == 4:
			image = Image.open("{}/sprites/pawns/smallMace_accessory{}.png".format(folder, cond))
			return self.generateSpritesheet(image, 4)



class Ranged(Item):
	def subInit(self, user, stats, handedness = 1):
		self.metaInit(user, stats, handedness, "item", "weapon", "ranged", True)

	def findenemy(self, filterAllies = True):
		arrCol = self.user.battle.arrCol.copy()
		lineCol = self.user.battle.lineCol.copy()
		pawnCol = self.user.battle.pawnCol.copy()

		boardSize = len(arrCol)
		arrHit = [[None for x in range(boardSize)] for y in range(boardSize)]
		for wai in range(boardSize):
			for eks in range(boardSize):
				if gridDist([self.user.x, self.user.y], [eks, wai]) >= 1 and clearline(arrCol, lineCol, [self.user.x, self.user.y], [eks, wai]) and octogonal(self.user.x - eks, self.user.y - wai):
					if pawnCol[eks][wai] != None:
						if (pawnCol[eks][wai].player.team != self.user.player.team) or not filterAllies:
							arrHit[eks][wai] = "enemy"
						else:
							arrHit[eks][wai] = "ally"
					else:
						arrHit[eks][wai] = "inrange"
		return arrHit


class bow(Ranged):
	def __init__(self, user = None, stats = {}):
		self.subInit(user, stats, 1)
		self.Atk = 1

	def imageGet(self, frame, cond = ""):
		if frame == 0:
			return Image.open("{}/sprites/icon_bow.png".format(folder))
		elif frame == 1:
			return Image.open("{}/sprites/pawns/bow_right{}.png".format(folder, cond))
		elif frame == 2:
			return Image.open("{}/sprites/pawns/bow_left{}.png".format(folder, cond))
		elif frame == 3:
			image = Image.open("{}/sprites/pawns/bow_accessory{}.png".format(folder, cond))
			return self.generateSpritesheet(image, 3)
		elif frame == 4:
			image = Image.open("{}/sprites/pawns/bow_accessory{}.png".format(folder, cond))
			return self.generateSpritesheet(image, 4)



class Potion(Item):
	def subInit(self, user, stats):
		self.metaInit(user, stats, 1, "item", "potion", "health", False)
		self.maxVol = self.stats.get("maxVol")
		self.Vol = self.stats.get("Vol")
		self.relay_useTime = 25

	def consume(self):
		if self.Vol <= 0:
			return True
		else:
			return False

	def drawStats(self):
		image = Image.new("RGBA", (23, 23), (0, 0, 0, 0))	#Black but invisible

		if round(self.Vol) > 99:
			Ox = 2
			writing = "_F"
		else:
			Ox = 2#2
			writing = str(math.ceil(self.Vol))
		stat1 = Image.open("{}/sprites/iconst_vol.png".format(folder))
		anglevar = math.floor((self.maxVol - self.Vol) / self.maxVol * 4)
		sheet_x1 = anglevar * 7
		sheet_y1 = 0
		sheet_x2 = (anglevar + 1) * 7
		sheet_y2 = 7
		crop_coords = (sheet_x1, sheet_y1, sheet_x2, sheet_y2)
		stat1 = stat1.crop(crop_coords)
		image.paste(stat1, (0 +Ox, 8), stat1)
		image = drawText(image, writing, "small", 8 +Ox, 9)

		return image


class healthPotion(Potion):
	def __init__(self, user = None, stats = {}):
		self.subInit(user, stats)
		self.maxVol = 8
		self.Vol = self.maxVol

	def use(self, target = None):
		toHeal = min(self.user.maxHp - self.user.Hp, self.Vol)
		self.relay_particleType = "heal"
		self.relay_particleCount = toHeal * 12.5

		if toHeal == 0:
			return "full"
		else:
			self.user.Hp += toHeal
			self.user.latestHp += toHeal
			self.Vol -= toHeal
			#Add lines to undo any 'face' commands and remove the item from the user's inventory if it becomes empty
			return "successturn"

	def imageGet(self, frame, cond = ""):
		if frame == 0:
			full = Image.open("{}/sprites/icon_healthPotion.png".format(folder))
			empty = Image.open("{}/sprites/icon_healthPotion_empty.png".format(folder))

			pixelLayers = [5, 7, 9, 9, 9, 9, 7, 7, 5, 3, 3, 3]
			Continue = True
			coveredVolume = 0
			pixelsFull = 0
			while Continue:
				if self.Vol / self.maxVol * sum(pixelLayers) > coveredVolume:
					coveredVolume += pixelLayers[pixelsFull]
					pixelsFull += 1
				else:
					Continue = False

			cropCoords = (0, 19 - pixelsFull, 25, 25)
			full = full.crop(cropCoords)
			empty.paste(full, (0, 19 - pixelsFull), full)
			return empty
		elif frame in [1,2,3,4]:
			image = Image.open("{}/sprites/pawns/emptyPotion_accessory{}.png".format(folder, cond))
			full = Image.open("{}/sprites/pawns/healthPotion_accessory{}.png".format(folder, cond))
			yeetPixelsFull = math.ceil(self.Vol / self.maxVol * 2) * 2
			full = full.crop((0, 26 - yeetPixelsFull, 47, 47))
			image.paste(full, (0, 26 - yeetPixelsFull), full)
			if frame == 1:
				return self.generateSpritesheet(image, 1)
			elif frame == 2:
				return self.generateSpritesheet(image, 2)
			elif frame == 3:
				return self.generateSpritesheet(image, 3)
			elif frame == 4:
				return self.generateSpritesheet(image, 4)


class Yeeter(Item):
	def __init__(self, element):
		self.element = element

class Charm(Item):
	def __init__(self, effect):
		self.effect = effect

class Magic():
	def __init__(self, Type):
		self.type = Type #"active" or "passive"

class Active(Magic):
	def __init__(self, element, shape = None):
		self.element = element
		self.shape = shape

class Passive(Magic):
	def __init__(self, effect, condition, direction):
		self.effect = effect
		self.condition = condition
		self.direction = direction

#section 3 items (itembottom)
