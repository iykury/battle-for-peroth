#!/bin/bash
# Auto-updater and runner for Battle for Peroth Bot.
UPDATE_INTERVAL=60

echo '---'

trap clean_exit SIGINT
clean_exit() {
  echo "Trying to exit cleanly"
  if [ -n "$bp_bot_pid" ]; then
    if kill $bp_bot_pid; then
      echo "Killed Battle for Peroth Bot ($bp_bot_pid)"
    else
      echo "Couldn't kill Battle for Peroth Bot ($bp_bot_pid)"
    fi
  fi
  echo "Bye."
  exit
}

unstaged_msg=1
while true; do
  head=`git rev-parse HEAD`
  if ! git fetch; then
    echo "Couldn't git fetch – connectivity issues?"
  fi
  fetch_head=`git rev-parse FETCH_HEAD`
  # if remote is newer, try to pull
  status=`git status -s`
  # status will be empty iff no files are unsynchronised
  if [ -n "$status" ]; then
    if [ "$unstaged_msg" -eq 1 ]; then
      echo "There are unstaged files, so the repo will not be pulled from"
      echo "Assuming you know what you're doing"
      unstaged_msg=0
    fi
  else
    unstaged_msg=1
    if [ ! "$head" = "$fetch_head" ]; then
      if ! git merge FETCH_HEAD; then
        echo "Couldn't merge – smells like trouble"
      else
        echo "Newest commit successfully pulled"
        if [ -n "$bp_bot_pid" ]; then
          kill $bp_bot_pid
          unset bp_bot_pid
          echo "Battle for Peroth Bot has been killed"
        fi
        echo "Restarting"
        exec $0
      fi
    fi
  fi
  if [ -z "$bp_bot_pid" ]; then
    echo "HEAD = $head"
    echo "Trying to run Battle for Peroth Bot now"
    ./bot.py &
    bp_bot_pid=$!
    echo "PID = $bp_bot_pid"
  fi
  counter=0
  while kill -0 $bp_bot_pid >&- 2>&-; do
    sleep 1
    counter=$[ $counter + 1 ]
    if [ $counter -eq $UPDATE_INTERVAL ]; then
      break
    fi
  done
  if ! kill -0 $bp_bot_pid >&- 2>&-; then
    echo 'Battle for Peroth Bot died :('
  fi
done
