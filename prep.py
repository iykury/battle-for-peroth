import asyncio, random, math, os, discord, string
from PIL import Image, ImageChops
import bfpMap, content, battle

preps = []

class Prep:
	def __init__(self, teams, lobbies, players, pawns, rules):
		for team in teams:
			team.channel.mode = "preparation"
		for player in players:
			player.Ready = False

		self.teams = teams
		self.lobbies = lobbies
		self.players = players
		self.pawns = pawns
		self.rules = rules

		self.Begin = False
		self.Countdown = 0

		asyncio.ensure_future(self.start_turn())

	async def start_turn(self):
		for team in self.teams:
			#mention = ""
			#for player in team.players:
			#	mention += player.member.mention + " "
			#await team.channel.channel.send("{}, prepare your army!".format(mention[:-1]))
			#for player in team.players:
			#	await self.broadcastPawns(player.member, team)
			#	await self.broadcastInventory(player.member, team)
			for player in team.players:
				await team.channel.channel.send("{}, prepare your army!".format(player.member.mention))
				await self.broadcastPawns(player.member, team)
			await self.broadcastShop(team)

	async def broadcast(self, message = None):
		for team in self.teams:
			await team.channel.channel.send(message)
		for lobby in self.lobbies:
			await lobby.send(message)
	
	async def on_message(self, message):

		from_player = False
		turnEnd = False

		for t in self.teams:
			if message.channel == t.channel.channel:
				team = t

		for p in self.players:
			if message.author == p.member and message.channel == p.team.channel.channel:
				from_player = True
				player = p
		#Switch which of the two following lines is commented to toggle whether the bot checks for who's sending the command
		#if self.pawns[self.current_turn].player.team.channel.channel == message.channel:
		if from_player:
			parts = message.content.lower().split(" ")
			Directions = ["north", "n", "northeast", "ne", "east", "e", "southeast", "se", "south", "s", "southwest", "sw", "west", "w", "northwest", "nw"]

			#Step 1: Parse command
			command = [[]]
			isCommand = True
			iC = -1	#Command Index
			expectation = "command "

			invSlots = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]
			commandsPawns = ["pawns", "p"]
			commandsInventory = ["inventory", "inv", "i"]
			commandsGive = ["give", "g"]
			commandsTake = ["take", "t"]
			commandsSwap = ["swap", "s"]
			commandsReady = ["ready"]
			commandsUnready = ["unready"]
			commands = commandsPawns + commandsInventory + commandsGive + commandsTake + commandsSwap + commandsReady + commandsUnready
			#Remember to add new alias lists to the full `commands` list!
			for i in range(len(parts)):
				commanded = False
				if parts[i] in commands and "command " in expectation: #if (parts[i] in commands and iC >= 0) and
					commanded = True
					check = parts[i]
					if iC >= 0:
						check = command[iC][0]
					iC += 1

					command.append([])
					command[iC].append(check)
					if check in commandsPawns:
						expectation = "command number-X "
					elif check in commandsInventory + commandsReady + commandsUnready:
						expectation = "command "
					elif check in commandsGive and not player.Ready:
						expectation = "any command "
					elif check in [commandsTake, commandsSwap] and not player.Ready:
						expectation = "pawnslot command "

				elif content.isInt(parts[i]) and "number-X " in expectation: #if
					command[iC].append(parts[i])
					expectation = "command number-X "

				elif content.isLetNum(parts[i]) and "any " in expectation:	#Item
					command[iC].append(parts[i])
					#newexpect = "any command "
					#expectation = newexpect
				elif content.isNumP(parts[i]) and ("any " in expectation or "pawnslot " in expectation):	#Pawn
					command[iC].append(parts[i])
					#newexpect = "any command "
					#expectation = newexpect
				elif parts[i] in invSlots and ("any " in expectation or "pawnslot " in expectation):		#Slot
					command[iC].append(parts[i])
					#newexpect = "any command "
					#expectation = newexpect

				else:	#elif not commanded:
					isCommand = False

			#Step 2: Fulfil command
			showPawns = False
			showPawnIndexes = []
			showInv = False
			showShop = False
			if isCommand:
				i = 0
				while i <= iC:
					if command[i][0] in commandsPawns:
						if len(command[i]) >= 2:
							showPawns = True
							for j in range(1, len(command[i])):
								if int(command[i][j]) not in showPawnIndexes:
									showPawnIndexes.append(int(command[i][j]))
						else:	#(len(player.pawns) - 1) // 6 + 2
							showPawns = True
							if 0 not in showPawnIndexes:
								showPawnIndexes.append(0)

					if command[i][0] in commandsInventory:
						showShop = True

					if command[i][0] in commandsGive:
						latestBuy = None
						listBuys = []
						useLastBuy = 0
						LastPawn = None
						useLastPawn = 0
						listPawns = []
						listSlots = []
						for j in range(1, len(command[i])):
							Input = command[i][j]
							error = False
							#1.1: Purchase
							if content.isLetNum(Input):
								if player.Points >= 1:
									purchase = content.buy(Input)
									if purchase != None:
										latestBuy = Input
										listBuys.append(Input)
										if useLastBuy > 0:
											useLastBuy *= -1
										if useLastBuy == 0:
											useLastBuy = -1
							#1.2: Pawn
							elif content.isNumP(Input):
								pawnIndex = content.simpPawnLabel(Input)
								if pawnIndex >= len(player.pawns):
									messages = ["Invalid input: Pawn input of range", "Must name an existing Pawn in `give` command.", "There is no Pawn labeled `"+Input+"`."]
									await message.channel.send(messages[random.randrange(len(messages))])
									error = True
								else:
									LastPawn = player.pawns[pawnIndex]
									listPawns.append(LastPawn)
									modPawnIndex = pawnIndex // 6 + 1
									if modPawnIndex not in showPawnIndexes:
										showPawnIndexes.append(modPawnIndex)
									useLastBuy += 1
									if useLastPawn > 0:
										useLastPawn *= -1
									if useLastPawn == 0:
										useLastPawn = -1
							#1.3: Slot
							elif Input in invSlots:
								if Input in ["1", "2", "3", "4", "9"]:
									listSlots.append(Input)
									useLastPawn += 1
								else:
									messages = ["Invalid input: Magic slot listed", "Cannot take magic abilities from Pawns.", "The listed Pawn item slot is a magic slot."]
									error = True

							#Step 2: Gather components from lists
							if len(listBuys) > 0:
								Item = content.buy(listBuys[0])
							elif useLastBuy > 0:
								Item = content.buy(latestBuy)
							else:
								Item = None

							if len(listPawns) > 0:
								Pawn = listPawns[0]
							elif useLastPawn > 0:
								Pawn = LastPawn
							else:
								Pawn = None

							if len(listSlots) > 0:
								Slot = listSlots[0]
							else:
								Slot = None

							#Step 3a: If no pawn item slot was indicated, decide for the player.
							if Item != None and Pawn != None and Slot == None:
								if Item.type == "weapon":
									if Pawn.checkItem(1) == None:
										Slot = "1"
									elif Pawn.checkItem(2) == None:
										Slot = "2"
									elif Pawn.checkItem(3) == None:
										Slot = "3"
									elif Pawn.checkItem(4) == None:
										Slot = "4"
									else:
										messages = ["Invalid input: No empty space", "Must give to a Pawn with empty inventory space.", "All of this Pawn's item slots are filled."]
										await message.channel.send(messages[random.randrange(len(messages))])
										error = True
								elif Item.type == "potion":
									if Pawn.checkItem(3) == None:
										Slot = "3"
									elif Pawn.checkItem(4) == None:
										Slot = "4"
									elif Pawn.checkItem(2) == None:
										Slot = "2"
									elif Pawn.checkItem(1) == None:
										Slot = "1"
									else:
										messages = ["Invalid input: No empty space", "Must give to a Pawn with empty inventory space.", "All of this Pawn's item slots are filled."]
										await message.channel.send(messages[random.randrange(len(messages))])
										error = True

							#Step 3b: If a pawn item slot *was* indicated, put it in it.
							if Item != None and Pawn != None and Slot != None:
								Pawn.give(Item, int(Slot))
								player.Points -= 1
								showPawns = True
								showShop = True
								if len(listPawns) > 0:
									listPawns.pop(0)
								if len(listBuys) > 0:
									listBuys.pop(0)
								if len(listSlots) > 0:
									listSlots.pop(0)

					if command[i][0] in commandsTake:
						LastPawn = None
						useLastPawn = 0
						listPawns = []
						listSlots = []
						for Input in command[i][1:len(command[i])]:
							error = False
							#Step 1: Identify Pawn & Slot
							#1.1: Pawn
							if content.isNumP(Input):
								pawnIndex = content.simpPawnLabel(Input)
								if pawnIndex >= len(player.pawns):
									messages = ["Invalid input: Pawn input of range", "Must name an existing Pawn in `take` command.", "There is no Pawn labeled `"+Input+"`."]
									await message.channel.send(messages[random.randrange(len(messages))])
									error = True
								else:
									LastPawn = player.pawns[pawnIndex]
									listPawns.append(LastPawn)
									modPawnIndex = pawnIndex // 6 + 1
									if modPawnIndex not in showPawnIndexes:
										showPawnIndexes.append(modPawnIndex)
									if useLastPawn > 0:
										useLastPawn *= -1
									if useLastPawn == 0:
										useLastPawn = -1
							#1.2: Slot
							if Input in invSlots:
								if Input in ["1", "2", "3", "4", "9"]:
									listSlots.append(Input)
									useLastPawn += 1
								else:
									messages = ["Invalid input: Magic slot listed", "Cannot sell Pawns' magic abilities.", "The listed Pawn item slot is a magic slot."]
									error = True

							#Step 2: Gather components from lists
							if len(listPawns) > 0:
								Pawn = listPawns[0]
							elif useLastPawn > 0:
								Pawn = LastPawn
							else:
								Pawn = None

							if len(listSlots) > 0:
								Slot = listSlots[0]
							else:
								Slot = None

							#Step 2.5: Return error messages for insufficient inputs
							if Pawn == None or Slot == None:
								error = True

							#Step 3: Evaluate item slot
							if Pawn != None and Slot != None:
								Item = Pawn.checkItem(int(Slot))
								if Item == None:
									messages = ["Invalid input: Named item slot empty", "Cannot take from an empty item slot.", "There is no item in the listed slot."]
									await message.channel.send(messages[random.randrange(len(messages))])
									error = True

							#Step 4: Take item from Pawn & put it in player's inventory
							if not error:
								Pawn.take(int(Slot))
								player.Points += 1
								showPawns = True
								showShop = True
								if len(listPawns) > 0:
									listPawns.pop(0)
								if len(listSlots) > 0:
									listSlots.pop(0)

					if command[i][0] in commandsReady:
						player.Ready = True
						await self.broadcast(player.member.name + " is ready!")

						#Check if all players are ready
						Begin = True
						for player in self.players:
							if not player.Ready:
								Begin = False
						if Begin:
							self.Begin = True
							self.Countdown = 10

					if command[i][0] in commandsUnready:
						player.Ready = False
						await self.broadcast(player.member.name + " is no longer ready.")

						if self.Begin:
							self.Begin = False
							await self.broadcast("Countdown aborted.")

					#section commandsend

					i += 1

			if showPawns:
				if 0 in showPawnIndexes:
					await self.broadcastPawns(message.author, team, 0)
				else:
					for j in showPawnIndexes:
						await self.broadcastPawns(message.author, team, j)
			if showInv:
				await self.broadcastInventory(message.author, team)
			if showShop:
				await self.broadcastShop(team, message.author)

			while self.Begin and self.Countdown >= 0:
				await asyncio.sleep(1)
				#Begin countdown
				if self.Countdown == 10:
					await self.broadcast("All players are now ready for battle! Countdown begins in 5 seconds.\nType `unready` if you still need more time.")

				if self.Countdown == 5:
					await self.broadcast("Battle beginning in:")

				if self.Countdown > 0 and self.Countdown <= 5:
					await self.broadcast("**{}...**".format(str(self.Countdown)))

				if self.Countdown == 0:
					battle.battles.append(battle.Battle(self.teams, self.lobbies, self.players, self.pawns, self.rules))
					preps.remove(self)

				#print(self.Countdown)
				self.Countdown -= 1

	async def broadcastPawns(self, author, team, page = 0):
		#Find player
		for p in self.players:
			if p.member == author and p.team == team:
				player = p

		if page == 0:
			for i in range(1, (len(player.pawns) - 1) // 6 + 2):
				pawnpage = self.drawPawnPage(author, team, i)
				name = "pawnpage-{}.png".format(random.randrange(1000000))
				path = "{}/tmp/{}".format(folder, name)
				pawnpage.save(path)
				with open(path, "rb") as f:
					await team.channel.channel.send(file = discord.File(f)) #Fufufu.
				asyncio.ensure_future(delete_after(60, path))
		else:
			pawnpage = self.drawPawnPage(author, team, page)
			name = "pawnpage-{}.png".format(random.randrange(1000000))
			path = "{}/tmp/{}".format(folder, name)
			pawnpage.save(path)
			with open(path, "rb") as f:
				await team.channel.channel.send(file = discord.File(f)) #Fufufu.
			asyncio.ensure_future(delete_after(60, path))

	def drawPawnPage(self, author, team, page):
		#Find player
		for p in self.players:
			if p.member == author and p.team == team:
				player = p

		cardXs = [2, 201]
		cardYs = [2, 102, 202]
		bottoms = [96, 198, 300]

		iMin = (page - 1) * 6
		iMax = page * 6
		bottom = bottoms[(len(player.pawns) - 1) // 2 % 3]

		image = Image.new("RGBA", (400, bottom), (54, 57, 63))	#Discord Gray
		cardFormat = Image.open("{}/sprites/infocard_prep.png".format(folder))

		for i in range(iMin, min(len(player.pawns), iMax + 1)):
			x = cardXs[i % 2]
			y = cardYs[(i // 2) % 3]
			if i == len(player.pawns) - 1 and len(player.pawns) % 2 == 1:
				x = 101

			image.paste(cardFormat, (x, y), cardFormat)

			pawn = player.pawns[i]
			color = self.determineColor(pawn, player.team)
			pawn.update_sprite(pawn.player.team == player.team, True)#, False)
			if pawn.player.team == player.team:
				pawn.draw_on(image, x - 14, y + 2, pawn.colored_images[color])
			else:
				pawn.draw_on(image, x - 15, y + 2, pawn.colored_images[color])

			#Draw base stats
			image = content.drawText(image, pawn.name, "large", x, y)
			centering = content.textWidth(str(i + 1) + "p", "large")
			image = content.drawText(image, str(i + 1) + "p", "large", x + 18 - centering // 2, y + 65)
			#Draw HP hearts
			hpLeftToDraw = pawn.maxHp
			hpDrawPencilX = 37
			fullHeart = Image.open("{}/sprites/heart_bigfull.png".format(folder))
			halfHeart = Image.open("{}/sprites/heart_bighalf.png".format(folder))
			while hpLeftToDraw > 0:
				if hpLeftToDraw >= 2:
					image.paste(fullHeart, (x + hpDrawPencilX, y + 18), fullHeart)
					hpLeftToDraw -= 2
					hpDrawPencilX += 10
				elif hpLeftToDraw == 1:
					image.paste(halfHeart, (x + hpDrawPencilX, y + 18), halfHeart)
					hpLeftToDraw -= 1
					hpDrawPencilX += 6

			#Draw items
			xCoordsIcons = [78, 78, 105, 105, 29, 48, 67, 76, 105]
			yCoordsIcons = [12, 39, 12, 39, 66, 66, 66, 66, 66]
			for j in range(len(pawn.inventory)):
				item = pawn.inventory[j]
				if item != None:
					icon = item.imageGet(0)
					image.paste(icon, (x + xCoordsIcons[j], y + yCoordsIcons[j]), icon)

		image = image.resize((800, bottom * 2))
		return image

	def determineColor(self, thisPawn, team = None):
		dictAllies = ["blue", "green", "yellow", "white"]
		dictEnemies0 = ["red", "orange", "black", "purple"]
		dictEnemies1 = ["black", "purple"]
		dictEnemies2 = ["white", "yellow"]
		dictEnemies3 = ["yellow"]
		dictEnemies4 = ["purple"]
		dictEnemies5 = ["orange"]
		dictEnemies6 = ["green"]
		dictEnemies = [dictEnemies0, dictEnemies1, dictEnemies2, dictEnemies3, dictEnemies4, dictEnemies5, dictEnemies6]
		latestTeam = None
		noEnemies = -1
		latestPlayer = None
		noPlayers = -1
		color = "white"
		isAlly = False

		for pawn in self.pawns:
			if pawn.player.team == team:
				isAlly = True
				if pawn.player.team != latestTeam:
					latestTeam = pawn.player.team
					noPlayers = -1
				if pawn.player != latestPlayer:
					noPlayers += 1
					latestPlayer = pawn.player
				if pawn == thisPawn:
					return dictAllies[noPlayers]
			else:
				isAlly = False
				if pawn.player.team != latestTeam:
					noEnemies += 1
					latestTeam = pawn.player.team
					noPlayers = -1
				if pawn.player != latestPlayer:
					noPlayers += 1
					latestPlayer = pawn.player
				if pawn == thisPawn:
					faction = dictEnemies[noEnemies]
					return faction[noPlayers]

	async def broadcastInventory(self, author, team):
		#Find player
		for p in self.players:
			if p.member == author and p.team == team:
				player = p

		inventory = self.drawInventory(author, team)
		name = "inventory-{}.png".format(random.randrange(1000000))
		path = "{}/tmp/{}".format(folder, name)
		inventory.save(path)
		with open(path, "rb") as f:
			await team.channel.channel.send(file = discord.File(f)) #Fufufu.
		asyncio.ensure_future(delete_after(60, path))

	def drawInventory(self, author, team):
		#Find player
		for p in self.players:
			if p.member == author and p.team == team:
				player = p

		#Determine size
		inv = player.inventory
		rows = max(len(player.inventory) - 1, 0) // 8 + 1
		size = rows * 29

		#Layer 1: Black background
		image = Image.new("RGBA", (400, size), (0, 0, 0))	#Black
		#image = Image.new("RGBA", (400, 116), (54, 57, 63))	#Discord Gray

		#Layer 2: Item icons
		for i in range(len(player.inventory)):
			item = player.inventory[i]
			x = 2 + 44 * (i % 8)
			y = 2 + 29 * (i // 8)
			if item != None:
				pasting = item.imageGet(0)
				image.paste(pasting, (x, y), pasting)

		#Layer 3: Card format
		cardFormat = Image.open("{}/sprites/inventory.png".format(folder))
		image.paste(cardFormat, (0, 0), cardFormat)

		#Layer 4: Item stats
		for i in range(len(player.inventory)):
			item = player.inventory[i]
			x = 27 + 44 * (i % 8)
			y = 3 + 29 * (i // 8)
			if item != None:
				pasting = item.drawStats()
				image.paste(pasting, (x, y), pasting)

		#Layer 5: Points
		coinsicon = Image.open("{}/sprites/coinsicon.png".format(folder))
		image.paste(coinsicon, (356, size-21))
		image = content.drawText(image, str(player.Points), "large", 384, size-20)

		image = image.resize((800, size * 2))
		return image

	async def broadcastShop(self, team, author = None):
		#Find player
		for p in self.players:
			if p.member == author and p.team == team:
				player = p

		shop = self.drawShop(team, author)
		name = "inventory-{}.png".format(random.randrange(1000000))
		path = "{}/tmp/{}".format(folder, name)
		shop.save(path)
		with open(path, "rb") as f:
			await team.channel.channel.send(file = discord.File(f)) #Fufufu.
		asyncio.ensure_future(delete_after(60, path))

	def drawShop(self, team, author = None):
		#Find player
		for p in self.players:
			if p.member == author and p.team == team:
				player = p

		#Layer 1: Black background
		image = Image.new("RGBA", (400, 98), (0, 0, 0))	#Black
		#image = Image.new("RGBA", (400, 116), (54, 57, 63))	#Discord Gray

		#Layer 2: Item icons
		pasting = Image.open("{}/sprites/icon_mace.png".format(folder))
		image.paste(pasting, (105, 2), pasting)
		pasting = Image.open("{}/sprites/icon_bow.png".format(folder))
		image.paste(pasting, (163, 16), pasting)
		pasting = Image.open("{}/sprites/icon_healthPotion.png".format(folder))
		image.paste(pasting, (82, 71), pasting)
		
		#Layer 3: Card format
		cardFormat = Image.open("{}/sprites/shop.png".format(folder))
		image.paste(cardFormat, (0, 0), cardFormat)

		#Layer 4: Points
		if author != None:
			coinsicon = Image.open("{}/sprites/coinsicon.png".format(folder))
			image.paste(coinsicon, (348, 8))
			image = content.drawText(image, str(player.Points), "large", 376, 9)

		image = image.resize((800, 98 * 2))
		return image

async def delete_after(time, filepath):
	await asyncio.sleep(time)
	os.remove(filepath)
