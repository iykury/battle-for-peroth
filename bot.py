#!/usr/bin/env python3
import discord, os, asyncio, sys, signal, random
import bfpQuickBattle, battle, content, prep

client = discord.Client()
bfpQuickBattle.client = client
battle.client = client

file_path = os.path.abspath(__file__)
folder = os.path.dirname(file_path)
bfpQuickBattle.folder = folder
battle.folder = folder
content.folder = folder
prep.folder = folder

token = None
try:
	with open("{}/token".format(folder)) as token_file:
		token = token_file.read().strip("\n")
except:
	print("Couldn't get token")

if not os.path.exists("{}/tmp".format(folder)):
	os.mkdir("{}/tmp".format(folder))
for f in os.listdir("{}/tmp".format(folder)):
	os.remove("{}/tmp/{}".format(folder, f))

first_login = True

servers = []

class Server:
	def __init__(self, guild):
		self.guild = guild
		self.players = []
		self.ws_lobby = None
		self.qb_lobby = None
		self.ws_channels = []
		self.qb_channels = []
		self.rules = {}

	def add_players(self, members):
		players = []
		for member in members:
			player = None
			for p in self.players:
				if member == p.member:
					player = p
			if player is None:
				player = Player(member)
				self.players.append(player)
			players.append(player)
		return players
	
	#mode = 0 for Warring States; mode = 1 for Quick Battle
	async def assign_channel(self, players, mode = 0):
		if mode == 1:
			channels = self.qb_channels
			lobby = self.qb_lobby
			prefix = "battle-game-"
			channel_type = "qb"
		else:
			channels = self.ws_channels
			lobby = self.ws_lobby
			prefix = "warring-states-"
			channel_type = "ws"
			
		#Set permissions for @everyone and the bot
		overwrites = {
			self.guild.default_role: discord.PermissionOverwrite(read_messages = False),
			self.guild.me: discord.PermissionOverwrite(read_messages = True),
		}
		
		#Set permission for the players
		for player in players:
			overwrites[player.member] = discord.PermissionOverwrite(read_messages = True)
		
		#Get lobby category
		if lobby is None:
			category = None
		else:
			category = lobby.category

		#Remove players from any channels they're already in   lol no
		#and find an empty channel if one exists
		channel = None
		for c in channels:
			if c.players == [] and channel is None:
				channel = c
			#for player in players:
			#	if player in c.players:
			#		c.players.remove(player)
			#		await c.channel.set_permissions(player.member, overwrite = None)
		
		#Create new channel
		if channel is None:
			if False:	#Random string version
				#Generate random channel name suffix
				alphanum = "0123456789abcdefghijklmnopqrstuvwxyz"
				suffix = ""
				for i in range(0, 4):
					suffix += random.choice(alphanum)
			if True:	#ordinal version
				suffix = str(len(channels) + 1)

			name = prefix + suffix
			channel = Channel(await self.guild.create_text_channel(
				name, overwrites = overwrites, category = category), players, self)
			channels.append(channel)
		#Reuse an empty channel
		else:
			#async for message in channel.channel.history(limit = None):	#these looked like they would have cleared all the messages in each channel, but i guess they don't -alexolas
				#await message.delete()
			for o in overwrites:
				await channel.channel.set_permissions(o, overwrite = overwrites[o])
			channel.players = players
		
		#Send message
		add_msg = "Added "
		for i in range(0, len(players)):
			if i == len(players) - 1:
				add_msg += "and "
			add_msg += players[i].member.mention + ", "
		add_msg += " to <#{}>".format(channel.channel.id)
		await channel.channel.send(add_msg)

		#Rewrite channels file
		with open("{}/servers/{}/{}_channels".format(folder, self.guild.id, channel_type), "a") as f:
			lines = []
			for c in channels:
				info = str(c.channel.id)
				for p in c.players:
					info += " " + str(p.member.id)
				info += "\n"
				lines.append(info)
			f.seek(0)
			f.truncate()
			f.writelines(lines)

		return channel

	#async def deleteChannels(self):


class Player:
	def __init__(self, member):
		self.member = member
		self.ws_channel = None
		self.qb_channel = None
		self.pawns = []

	def outOfPawns(self):
		result = True
		for Pawn in self.pawns:
			if Pawn.turnActive:
				result = False
		return result

class Channel:
	def __init__(self, channel, players, server):
		self.channel = channel
		self.players = players
		self.server = server

@client.event
async def on_ready():
	print("Logged in as")
	print(client.user.name)
	print(client.user.id)
	print("------") 
	
	if not os.path.exists("{}/servers".format(folder)):
		os.mkdir("{}/servers".format(folder))

	global first_login, lobby
	if first_login:
		#Create missing server folders
		server_folders = os.listdir("{}/servers".format(folder))
		for guild in client.guilds:
			if not str(guild.id) in server_folders:
				os.mkdir("{}/servers/{}".format(folder, guild.id))
				os.mkdir("{}/servers/{}/players".format(folder, guild.id))
		#Load servers
		if os.path.exists("{}/servers".format(folder)):
			for server_id in os.listdir("{}/servers".format(folder)):
				server = Server(client.get_guild(int(server_id)))
				#Load players
				for player in os.listdir("{}/servers/{}/players".format(folder, server_id)):
					member = server.guild.get_member(int(player))
					server.players.append(Player(member))
				#Load channels
				for mode in ["ws", "qb"]:
					try:
						with open("{}/servers/{}/{}_channels".format(folder, server_id, mode)) as f:
							for line in f.readlines():
								try:
									ids = line.split(" ")
									channel_id = int(ids[0])
									player_ids = ids[1:]
									players = []
									for player_id in player_ids:
										member = server.guild.get_member(int(player_id))
										if not member is None:
											players.append(Player(member))
									channel = client.get_channel(channel_id)
								except ValueError:
									pass
								else:
									if not channel is None:
										if mode == "ws":
											server.ws_channels.append(Channel(channel, players, server))
										if mode == "qb":
											server.qb_channels.append(Channel(channel, players, server))
					except FileNotFoundError:
						pass
					try:
						with open("{}/servers/{}/{}_lobby".format(folder, server_id, mode)) as f:
							try:
								lobby_id = int(f.read().strip("\n"))
								lobby = client.get_channel(lobby_id)
								if mode == "ws":
									server.ws_lobby = lobby
								elif mode == "qb":
									server.qb_lobby = lobby
							except ValueError:
								pass
					except FileNotFoundError:
						pass
				#Add server to list of servers
				servers.append(server)
		
		##Since it isn't working properly yet, I'll just hardcode the data in
		#server = Server(client.guilds[0])
		#iyk = Player(server.guild.get_member(201874260787068928))
		#alt = Player(server.guild.get_member(551882662785187875))
		#alex = Player(server.guild.get_member(266358272150339584))
		#server.players = [iyk, alt]
		#server.qb_channels = [
		#	Channel(client.get_channel(574040462340390922), [iyk], server),
		#	Channel(client.get_channel(574040480019644417), [alt], server)
		#]
		#iyk.qb_channel = server.qb_channels[0]
		#alt.qb_channel = server.qb_channels[1]
		#server.qb_lobby = client.get_channel(566744850255314944)
		#servers.append(server)
		##mentions = ""
		##for player in servers[0].players:
		##    mentions += player.member.mention
		##await servers[0].qb_lobby.send("This is the QB lobby and {} are all of the players in the server".format(mentions))
		##for channel in servers[0].qb_channels:
		##    mentions = ""
		##    for player in channel.players:
		##        mentions += player.member.mention
		##    await channel.channel.send("This is a QB channel and {} are all of the players in this channel".format(mentions))

		first_login = False

aliases = {
	"qb-lobby": ["qb-lobby", "qblobby"]
}

@client.event
async def on_message(message):
	global lobby
	#Bot won't reply to itself
	if message.author == client.user:
		return
	
	parts = message.content.split(" ")
	command = None
	if parts[0].startswith("!"):
		command = parts[0][1:]
	
	for s in servers:
		if message.guild == s.guild:
			server = s

	if command in aliases["qb-lobby"]:
		server.qb_lobby = message.channel
		await message.channel.send("Battle Game Lobby is now on <#{}>".format(message.channel.id))
		
		with open("{}/servers/{}/qb_lobby".format(folder, message.guild.id), "w") as f:
			f.write(str(message.channel.id))
	
	#Find if the message is from a private channel
	ws_channel = False
	for channel in server.ws_channels:
		if message.channel == channel.channel:
			ws_channel = True
			break

	qb_channel = False
	for channel in server.qb_channels:
		if message.channel == channel.channel:
			qb_channel = True
			break
	
	#<TEST COMMANDS>
	if command == "chan":
		await server.assign_channel(server.add_players(message.mentions), 1)
	
	if command == "listall":
		for c in server.qb_channels:
			mentions = ""
			for p in c.players:
				mentions += p.member.mention
			await c.channel.send("<#{}>: {}".format(c.channel.id, mentions))

	if command == "activebattles":
		await message.channel.send("{} battle(s) currently active.".format(str(len(battle.battles))))

	if command == "clearall":
		for c in server.qb_channels:
			c.players = []
		await message.channel.send("Channels cleared.")

	if command == "pawns":
		if len(parts) >= 2:
			server.rules['pawns per player'] = int(parts[1])
			await message.channel.send("Pawns Per Player set to " + parts[1])
		else:
			await message.channel.send("ERROR: No argument given.")
	#</TEST COMMANDS>
	
	if command == "start":
		#Start a battle
		bfpQuickBattle.games.append(bfpQuickBattle.Game(server.qb_channels + [], server.rules))

	#UTILITY COMMANDS
	Teams = []
	Lobbies = []
	for chan in servers[0].qb_channels:
		Teams.append(chan)
		if not chan.server.qb_lobby in Lobbies:
			Lobbies.append(chan.server.qb_lobby)
	from_lobby = False
	from_team = False
	if message.channel in Lobbies:
		from_lobby = True
	for chan in Teams:
		if message.channel == chan.channel:
			from_team = True
			Chan = chan
			break
	#Identify modes
	for metaChan in prep.preps + battle.battles:
		for thatChan in metaChan.teams:
			for thisChan in Teams:
				if thatChan.channel.channel == thisChan.channel:
					thisChan.mode = thatChan.channel.mode

	#Chat command
	if message.content.startswith("$") and (from_lobby or from_team):
		msg = "<{}> {}".format(message.author.name, message.clean_content[1:])
		sentTo = []
		for chan in Teams:
			if chan.channel != message.channel and not chan.channel in sentTo:
				await chan.channel.send(msg)
				sentTo.append(chan.channel)
		for lobby in Lobbies:
			if lobby != message.channel and not lobby in sentTo:
				await lobby.send(msg)
				sentTo.append(lobby)

	#Help command
	parts = message.content.split(" ")
	commandsHelp = ["help", "whatis", "whatare"]
	if parts[0] in commandsHelp:
		if from_lobby:
			category = "lobby"
		elif from_team:
			category = Chan.mode
		for i in range(1,len(parts)):
			advice = content.help(parts[i], category, parts[0])
			if advice != None:
				await message.channel.send(advice)
		if len(parts) == 1:
			advice = content.help("topics", category, parts[0])
			if advice != None:
				await message.channel.send(advice)

	#Send message to battle code
	for b in bfpQuickBattle.games:
		await b.on_message(message)

def exit_gracefully(sig, frame):
	print("Trying to exit gracefully")
	asyncio.ensure_future(client.logout())
	# Python should stop once all the tasks are cancelled
	# yadda yadda – code will go here
	sys.exit(0)

signal.signal(signal.SIGINT, exit_gracefully)

client.run(token)
