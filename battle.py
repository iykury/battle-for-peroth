import asyncio, random, math, os, discord, string
from PIL import Image, ImageChops
import bfpMap, content

battles = []

class Battle:
	def __init__(self, teams, lobbies, players, pawns, rules):
		for team in teams:
			team.channel.mode = "battle"
		self.teams = teams
		self.lobbies = lobbies
		self.players = players
		self.pawns = pawns
		self.rules = rules

		#All of these should be determined in the lobby
		self.redFog = rules.get('red fog')
		self.respawnRule = rules.get('respawns', 2) #0 = None; 1 = Low; 2 = Normal; 3 = High

		for pawn in self.pawns:
			pawn.putIn(self)

		self.boardSize = min(content.boardsizeCal(len(self.pawns)), 12)	#Remove "min( ... , 12)" when we can have boards of sizes > 12
		package = bfpMap.generate(self.boardSize)
		self.arrCol = package['collision']
		self.lineCol = package['linecol']
		self.pawnCol = [[None for x in range(self.boardSize)] for y in range(self.boardSize)]
		self.map = package['floor']
		self.mapL1 = package['layer1']

		#Algorithm to randomly place new pawns at the edges of the map
		#Step 1: Create an array of the entire perimeter
		arrPeri = []
		#1.1: Top
		for i in range(self.boardSize - 1):
			if self.arrCol[i][0] == "clear":
				arrPeri.append((i, 0))
		#1.2: Right
		for i in range(self.boardSize - 1):
			if self.arrCol[self.boardSize - 1][i] == "clear":
				arrPeri.append((self.boardSize - 1, i))
		#1.3: Bottom
		for i in range(self.boardSize - 1):
			if self.arrCol[self.boardSize - 1 - i][self.boardSize - 1] == "clear":
				arrPeri.append((self.boardSize - 1 - i, self.boardSize - 1))
		#1.4: Left
		for i in range(self.boardSize - 1):
			if self.arrCol[0][self.boardSize - 1 - i] == "clear":
				arrPeri.append((0, self.boardSize - 1 - i))
		#Step 2: Establish number of divisions
		spawnpoints = 0
		for team in self.teams:
			for player in team.players:
				spawnpoints += 1
			spawnpoints += 1
		placement = random.randrange(len(arrPeri))
		#Step 3: Assign pawns their spawn points
		for team in self.teams:
			for player in team.players:
				for pawn in player.pawns:
					pawn.x = arrPeri[math.floor(placement)][0]
					pawn.oriX = pawn.x
					pawn.y = arrPeri[math.floor(placement)][1]
					pawn.oriY = pawn.y
					pawn.angle = content.find_octant((self.boardSize - 1) / 2, (self.boardSize - 1) / 2, pawn.x, pawn.y)
					pawn.oriAngle = pawn.angle
				placement = (placement + len(arrPeri) / spawnpoints) % len(arrPeri)
			placement = (placement + len(arrPeri) / spawnpoints) % len(arrPeri)

		#Respawning
		teamRespawns = [0, 1,   2, 4]	#0 = None
		plyrRespawns = [0, 1.5, 3, 5.5]	#1 = Low
		pawnRespawns = [0, 2,   4, 7]	#2 = Normal
		for pawn in self.pawns:			#3 = High
			pawn.respawns = pawnRespawns[self.respawnRule]
		for player in self.players:
			player.respawns = plyrRespawns[self.respawnRule] * len(player.pawns)
		for team in self.teams:
			team.respawns = teamRespawns[self.respawnRule] * team.noPawns()

		self.currentPlayer = -1
		self.nextTurn()
		asyncio.ensure_future(self.start_turn())

	def nextTurn(self):
		self.currentPlayer = (self.currentPlayer + 1) % len(self.players)
		for Pawn in self.players[self.currentPlayer].pawns:
			if Pawn.Hp > 0:
				Pawn.turnActive = True
		self.current_turn = -1
	
	async def start_turn(self):
		#Advance animations
		for pawn in self.pawns:
			if "-0" in pawn.spriteState:
				pawn.spriteState = pawn.spriteState[:-2]
			else:
				pawn.spriteState = "normal"
			if pawn.deathState in [1, 2]:
				pawn.deathState += 1
			if pawn.deathState == 3:
				pawn.respawn()
			if "-0" in pawn.particleType:
				pawn.particleType = pawn.particleType[:-2]
			else:
				pawn.particleType = ""

		#Check for victory
		pawns = self.pawns.copy()
		team1 = None
		stalemate = True
		victory = False
		continuation = False
		for i in range(len(pawns)):
			pawnIsAlive = (pawns[i].deathState == 0 or (pawns[i].respawns > 0 and pawns[i].player.respawns > 0 and pawns[i].player.team.respawns > 0))
			#Find a pawn
			if team1 == None and pawnIsAlive:
				team1 = pawns[i].player.team
				stalemate = False
				victory = True
			#Compare teams
			else:
				if pawns[i].player.team != team1 and pawnIsAlive:
					victory = False
					continuation = True
		#Announce outcome
		if stalemate:
			await self.broadcast_field(False)
			msg = "STALEMATE! All teams have been wiped from the map; No victor can be declared."
			for team in self.teams:
				await team.channel.channel.send(msg)
			for lobby in self.lobbies:
				await lobby.send(msg)
			battles.remove(self)
		if victory:
			await self.broadcast_field(False)
			if len(team1.players) == 1:
				msgWinners = team1.players[0].member.name
				msg = "VICTORY! {} has won the game!".format(msgWinners)
			elif len(team1.players) == 2:
				msgWinners = team1.players[0].member.name + " and " + team1.players[1].member.name
				msg = "VICTORY! {} have won the game!".format(msgWinners)
			else:
				msgWinners = ""
				for i in range(len(team1.players)):
					if len(team1.players) - i == 1:
						msgWinners += team1.players[i].member.name
					elif len(team1.players) - i == 2:
						msgWinners += team1.players[i].member.name + ", and "
					else:
						msgWinners += team1.players[i].member.name + ", "
				msg = "VICTORY! {} have won the game!".format(msgWinners)
			for team in self.teams:
				await team.channel.channel.send(msg)
			for lobby in self.lobbies:
				await lobby.send(msg)
			battles.remove(self)

		if continuation:
			await self.players[self.currentPlayer].team.channel.channel.send("{}'s turn".format(self.players[self.currentPlayer].member.mention))
			#Determine if player has only 1 pawn available
			availablePawns = []
			for pawn in self.players[self.currentPlayer].pawns:
				if pawn.turnActive:
					availablePawns.append(pawn)
			if len(availablePawns) == 1:
				foundPawn = availablePawns[0]
				self.current_turn = foundPawn.id_Player
				await self.players[self.currentPlayer].team.channel.channel.send("Now controlling " + foundPawn.name + ".")
			#Continue as before
			await self.broadcast_field()
			if self.current_turn > -1:
				await self.broadcast_infocard(self.players[self.currentPlayer].pawns[self.current_turn], [self.players[self.currentPlayer].team], self.players[self.currentPlayer])
	
	async def on_message(self, message):
		from_current_player = False
		turnEnd = False

		if self.players[self.currentPlayer].member == message.author:
			from_current_player = True
			Player = self.players[self.currentPlayer]
		#Switch which of the two following lines is commented to toggle whether the bot checks for who's sending the command
		#if self.pawns[self.current_turn].player.team.channel.channel == message.channel:
		if from_current_player:
			parts = message.content.split(" ")
			Directions = ["north", "n", "northeast", "ne", "east", "e", "southeast", "se", "south", "s", "southwest", "sw", "west", "w", "northwest", "nw"]

			#Step 1: Parse command
			command = [[]]
			isCommand = True
			iC = -1	#Command Index
			invslots = ["1", "2", "3", "4", "5", "6", "7", "8", "9"]

			expectation = "command "
			commandsMove = ["move", "go", "goto", "m", "g"]
			commandsTurn = ["turn", "rotate", "face", "f"]
			commandsWait = ["wait", "idle", "stop", "end", "x"]
			commandsReload = ["reload", "refresh"]
			commandsInfo = ["info", "check", "i"]
			commandsAttack = ["attack", "a"]
			commandsUse = ["use", "u"]	#might change in future (u -> up)
			commandsDefend = ["defend", "block", "shield", "guard", "b"]
			#todo:
			commandsSelect = ["select", "choose", "s"]	#might change in future (s -> south)
			commandsSelectInfo = ["selectinfo", "selectcheck", "chooseinfo", "choosecheck", "infoselect", "infochoose", "checkselect", "checkchoose", "si", "is"]
			commandsCounter = ["counter", "overwatch", "c"]
			commandsSwap = ["swap", "switch", "sw"]
			commandsThrow = ["throw", "toss", "t"]
			commands = commandsMove + commandsTurn + commandsWait + commandsReload + commandsInfo + commandsAttack + commandsUse + commandsDefend + commandsSelect
			#Remember to add new alias lists to the full `commands` list!
			for i in range(len(parts)):
				if parts[i] in commands and "command " in expectation:
					iC += 1
					command.append([])
					command[iC].append(parts[i])
					if parts[i] in commandsMove:
						expectation = "tile-X "
					elif parts[i] in commandsTurn:
						expectation = "tile-1 direction-1 "
					elif parts[i] in commandsWait + commandsReload + commandsDefend:
						expectation = "command "
					elif parts[i] in commandsInfo:
						expectation = "tile-X "
					elif parts[i] in commandsAttack:
						expectation = "tile-1 invslot-2 "
					elif parts[i] in commandsUse:
						expectation = "invslot-1 tile-2 "
					elif parts[i] in commandsSelect:
						expectation = "tile-1 "

				elif content.isCoord(parts[i]) and "tile-1 " in expectation:
					command[iC].append(parts[i])
					newexpect = ""
					if "invslot-2" in expectation:
						newexpect += "invslot-1 "
					newexpect += "command "
					expectation = newexpect

				elif content.isCoord(parts[i]) and "tile-X " in expectation:
					command[iC].append(parts[i])
					expectation = "command tile-X "

				elif content.isCoord(parts[i]) and "tile-2 " in expectation:
					command[iC].append(parts[i])
					newexpect = ""
					if "invslot-1 " in expectation:
						newexpect += "invslot-1 "
					else:
						newexpect += "command "
					expectation = newexpect

				elif parts[i] in Directions and "direction-1" in expectation:
					command[iC].append(parts[i])
					expectation = "command "	#Will need to be rewritten

				elif parts[i] in invslots and "invslot-1 " in expectation:
					command[iC].append(parts[i])
					newexpect = ""
					if "tile-2 " in expectation:
						newexpect += "tile-1 "
					newexpect += "command "
					expectation = newexpect

				else:
					isCommand = False

			#Step 2: Fulfil command
			newTurn = True
			if isCommand:
				i = 0
				while i <= iC and turnEnd == False:
					if self.current_turn >= 0:
						Pawn = Player.pawns[self.current_turn]

						if command[i][0] in commandsMove:
							for j in range(1, len(command[i])):
								xy = content.coords(command[i][j])
								x = xy[0]
								y = xy[1]
								move = Pawn.move(x, y)
								if move is "far":
									messages = ["Invalid move: Out of range", "You cannot move there yet.", "That place is too far away."]
									await message.channel.send(messages[random.randrange(len(messages))])
								elif move is "blocked":
									messages = ["Invalid move: Destination blocked", "You cannot move there.", "That place is blocked."]
									await message.channel.send(messages[random.randrange(len(messages))])
								elif move is "pawnblocked":
									messages = ["Invalid move: Destination blocked", "You cannot move there right now.", "Another Pawn is already standing there."]
									await message.channel.send(messages[random.randrange(len(messages))])
								elif move is "you":
									messages = ["Invalid move: Already at destination", "You are already standing there.", "That is you."]
									await message.channel.send(messages[random.randrange(len(messages))])
								elif move is "move":
									turnEnd = True

						if command[i][0] in commandsWait:
							Pawn.wait()
							turnEnd = True

						if command[i][0] in commandsTurn:
							if command[i][1] in Directions:
								result = Pawn.turnDir(command[i][1])
							elif content.isCoord(command[i][1]):
								xy = content.coords(command[i][1])
								x = xy[0]
								y = xy[1]
								result = Pawn.turn(x, y)
							if result == "phantomstate":
								messages = ["Invalid move: Cannot exit Phantom State", "You are in Phantom State; Doing that now would make you overlap with another Pawn."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "no":
								messages = ["Invalid move: Cannot self-target", "You cannot look at yourself.", "That is you."]
								await message.channel.send(messages[random.randrange(len(messages))])

						if command[i][0] in commandsAttack:
							xy = content.coords(command[i][1])
							x = xy[0]
							y = xy[1]
							if len(command[i]) == 2:
								result = await Pawn.attack(x, y)
							elif len(command[i]) == 3:
								result = await Pawn.attack(x, y, Pawn.inventory[int(command[i][2])])

							if result == "success":
								turnEnd = True
							elif result == "outofrange":
								messages = ["Invalid move: Out of range", "You can only attack tiles within your weapon's range.", "That target is out of range."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "noweapons":
								messages = ["Invalid move: No weapons available", "You need an item to attack with.", "None of your weapons can attack this target."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "needsheld":
								messages = ["Invalid move: Not holding weapon", "You need to be holding this weapon to use it."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "notaweapon":
								messages = ["Invalid move: No weapon selected", "You can only attack with weapons.", "That is not a weapon."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "notanitem":
								messages = ["Invalid move: No weapon selected", "You need a weapon in order to attack.", "That inventory slot is empty."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "nosuicide":
								messages = ["Invalid move: Cannot self-target", "You cannot attack yourself.", "That is you."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "phantomstate":
								messages = ["Invalid move: Cannot exit Phantom State", "You are in Phantom State; Doing that now would make you overlap with another Pawn."]
								await message.channel.send(messages[random.randrange(len(messages))])

						if command[i][0] in commandsUse:
							#Decipher inputs (can be [item] or [item][tile] or [tile][item])
							if len(command[i]) == 2:
								Item = command[i][1]
								Tile = None
							else:
								if command[i][1] in invslots:
									Item = command[i][1]
									Tile = command[i][2]
								else:
									Item = command[i][2]
									Tile = command[i][1]

							result = Pawn.use(Item, Tile)

							if result == "noitem":
								messages = ["Invalid move: No item selected", "You need to specify an item to use.", "You cannot use nothingness."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "needsheld":
								messages = ["Invalid move: Not holding item", "You need to be holding this item to use it."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "notarget":
								messages = ["Invalid move: No target", "You need to specify a target with this item.", "This item needs a target to use."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "full":
								messages = ["Invalid move: HP full", "You can't use healing items when already at max HP."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "phantomstate":
								messages = ["Invalid move: Cannot exit Phantom State", "You are in Phantom State; Doing that now would make you overlap with another Pawn."]
								await message.channel.send(messages[random.randrange(len(messages))])
							elif result == "success":
								turnEnd = True

						if command[i][0] in commandsDefend:
							Pawn.defend()
							turnEnd = True

						#newcommand

					# ^ Action commands
					# v Control commands

					if command[i][0] in commandsReload:
						newTurn = False
						await self.broadcast_field(True, [self.chan2team(message.channel)])
						#await self.broadcast_infocard(self.pawns[self.current_turn], [self.chan2team(message.channel)], self.pawns[self.current_turn].player)

					if command[i][0] in commandsInfo:
						newTurn = False
						for j in range(1, len(command[i])):
							xy = content.coords(command[i][j])
							x = xy[0]
							y = xy[1]
							if self.pawnCol[x][y] != None:
								await self.broadcast_infocard(self.pawnCol[x][y], [self.chan2team(message.channel)], Player)
							else:
								messages = ["Invalid move: No Pawn to check", "`{}` must be used on a tile with a Pawn.".format(command[i][0]), "No Pawn to check."]
								await message.channel.send(messages[random.randrange(len(messages))])

					if command[i][0] in commandsSelect:
						newTurn = False
						broadcastIC = False #IC = Infocard
						xy = content.coords(command[i][1])
						x = xy[0]
						y = xy[1]
						candidates = []
						for pPawn in Player.pawns:
							if pPawn.x == x and pPawn.y == y:
								candidates.append(pPawn)
						if len(candidates) == 0:
							messages = ["Invalid move: No Pawn to select", "`{}` must be used on a tile with one of your Pawns.".format(command[i][0]), "No Pawn to select."]
							await message.channel.send(messages[random.randrange(len(messages))])
						else:
							foundPawn = candidates[0]
							if foundPawn.player == Player:
								if foundPawn.turnActive:
									self.current_turn = foundPawn.id_Player
									await message.channel.send("Now controlling " + foundPawn.name + ".")
									broadcastIC = True
								else:
									messages = ["Invalid move: Pawn has already acted", "Pawns can only take one action per turn.", "{} has already acted.".format(foundPawn.name)]
									await message.channel.send(messages[random.randrange(len(messages))])	
							else:
								messages = ["Invalid move: Pawn belongs to a different player", "You can only select Pawns that belong to you.", "{} isn't your Pawn!".format(foundPawn.name)]
								await message.channel.send(messages[random.randrange(len(messages))])
						await self.broadcast_field(True, [self.chan2team(message.channel)])
						if broadcastIC:
							await self.broadcast_infocard(foundPawn, [self.chan2team(message.channel)], Player)

					#newcommand

					if turnEnd:
						Pawn.turnActive = False
						Pawn.latestWeapon = None
						Pawn.latestHp = Pawn.Hp
						# v Entirely related to Damage Stun immunity
						Pawn.wasImmuneToDamageStunFrom.append(Pawn.immuneToDamageStunFrom)
						Pawn.immuneToDamageStunFrom = -1
						if len(Pawn.willbeImmuneToDamageStunFrom) > 0:
							Pawn.immuneToDamageStunFrom = Pawn.willbeImmuneToDamageStunFrom[0]
							Pawn.willbeImmuneToDamageStunFrom.pop(0)
						else:
							Pawn.wasImmuneToDamageStunFrom = []
						# v Related to Defend degradation
						if Pawn.defendState == 0:
							Pawn.defendHp = min(Pawn.defendHp + 0.5, 3)

						self.current_turn = -1
						if Player.outOfPawns():
							self.nextTurn()

					i += 1

				if newTurn:
					await self.start_turn()


	async def broadcast(self, message = None, filename = None):
		for team in self.teams:
			with open(filename, "rb") as f:
				await team.channel.channel.send(message, file = discord.File(f))
		for lobby in self.lobbies:
			with open(filename, "rb") as f:
				await lobby.send(message, file = discord.File(f))
	
	def draw_field(self, team = None, includeOverlays = True):
		image = self.map.copy()
		
		#Draw grid overlay
		mapgridFull = Image.open("{}/sprites/overlay_board.png".format(folder))
		mapgrid = mapgridFull.crop((0, 0, self.boardSize * 32 + 2, self.boardSize * 24 + 2))
		mapgridBands = mapgrid.split()
		mapgridBands[3].paste(mapgridBands[3].point(lambda i: i * 0.75)) #Multiply pixels in band 3 (alpha) by 75%
		mapgrid = Image.merge("RGBA", mapgridBands)
		image.alpha_composite(mapgrid, (7 + 192 - 16 * self.boardSize, 5 + 144 - 12 * self.boardSize))
		
		if self.players[self.currentPlayer].pawns[self.current_turn].player.team is team and includeOverlays:
			#Draw overlays
			overlays = Image.new("RGBA", (400, 300))
			for y in range(0, self.boardSize):
				for x in range(0, self.boardSize):
					#Draw movement overlays
					if self.current_turn > -1:
						state = self.players[self.currentPlayer].pawns[self.current_turn].arrMov[x][y]
						if not state in ["far", "blocked", "pawnblocked"]:
							overlay = content.Sprite(Image.open("{}/sprites/overlay_{}.png".format(folder, state)), x, y, battle = self)
							overlay.draw_on_tile(overlays)
					#Draw combat overlays
					if self.current_turn > -1:
						state = self.players[self.currentPlayer].pawns[self.current_turn].arrTarg[x][y]
						if not state in [""]:
							overlay = content.Sprite(Image.open("{}/sprites/overlay_{}.png".format(folder, state)), x, y, battle = self)
							overlay.draw_on_tile(overlays)
					#Draw selection overlays
					pawnPresent = False
					for Pawn in self.players[self.currentPlayer].pawns:
						if Pawn.x == x and Pawn.y == y and Pawn.turnActive and Pawn.id_Player != self.current_turn:
							pawnPresent = True
					if pawnPresent:
						overlay = content.Sprite(Image.open("{}/sprites/overlay_youOrange.png".format(folder)), x, y, battle = self)
						overlay.draw_on_tile(overlays)
			overlaysBands = overlays.split()
			overlays = Image.merge("RGBA", overlaysBands)
			image.alpha_composite(overlays)
		
		#Draw first map layer above Pawns
		image.alpha_composite(self.mapL1)

		#Draw labels overlay
		maplabelsFull = Image.open("{}/sprites/overlay_boardlabels.png".format(folder))
		maplabels = maplabelsFull.crop((0, 0, self.boardSize * 32 + 2, self.boardSize * 24 + 2))
		maplabelsBands = maplabels.split()
		maplabelsBands[3].paste(maplabelsBands[3].point(lambda i: i * 0.75)) #Multiply pixels in band 3 (alpha) by 75%
		maplabels = Image.merge("RGBA", maplabelsBands)
		image.alpha_composite(maplabels, (7 + 192 - 16 * self.boardSize, 5 + 144 - 12 * self.boardSize))

		#Draw Pawns
		dictAllies = ["blue", "green", "yellow", "white"]
		dictEnemies0 = ["red", "orange", "black", "purple"]
		dictEnemies1 = ["black", "purple"]
		dictEnemies2 = ["white", "yellow"]
		dictEnemies3 = ["yellow"]
		dictEnemies4 = ["purple"]
		dictEnemies5 = ["orange"]
		dictEnemies6 = ["green"]
		dictEnemies = [dictEnemies0, dictEnemies1, dictEnemies2, dictEnemies3, dictEnemies4, dictEnemies5, dictEnemies6]
		color = "white"
		isAlly = False
		
		if team in self.teams:
			enemyTeams = self.teams.copy()
			enemyTeams.remove(team)
		
		pawnOrder = sorted(self.pawns, key=lambda pawn: pawn.y)
		for pawn in pawnOrder:
			if team in self.teams:
				if pawn.player.team == team:
					isAlly = True
					color = dictAllies[team.players.index(pawn.player)]
				else:
					isAlly = False
					faction = dictEnemies[enemyTeams.index(pawn.player.team)]
					color = faction[pawn.player.team.players.index(pawn.player)]
			elif team in self.lobbies:
				color = pawn.player.color
			pawn.update_sprite(isAlly)
			pawn.draw_on_tile(image, pawn.colored_images[color], True)

		#Resize image
		image = image.resize((800, 600))

		return image
	
	async def broadcast_field(self, gameOn = True, where = None):
		if where == None:
			where = self.teams + self.lobbies

		if gameOn:# and self.current_turn > -1:
			self.players[self.currentPlayer].pawns[self.current_turn].pathfind()
			self.players[self.currentPlayer].pawns[self.current_turn].targetfind()

		for chan in where:
			field = self.draw_field(chan, gameOn)
			name = "field-{}.png".format(random.randrange(1000000))
			path = "{}/tmp/{}".format(folder, name)
			field.save(path)
			with open(path, "rb") as f:
				if chan in self.teams:
					await chan.channel.channel.send(file = discord.File(f)) #Heh.
					#if self.pawns[self.current_turn].player in channel.channel.players:
					#	await channel.channel.channel.send("test")
				elif chan in self.lobbies:
					await chan.send(file = discord.File(f))
				else:
					await chan.send(file = discord.File(f))
			asyncio.ensure_future(delete_after(60, path))

	def chan2team(self, chan):
		for team in self.teams:
			if team.channel.channel == chan:
				return team

	def determineColor(self, thisPawn, team = None):
		dictAllies = ["blue", "green", "yellow", "white"]
		dictEnemies0 = ["red", "orange", "black", "purple"]
		dictEnemies1 = ["black", "purple"]
		dictEnemies2 = ["white", "yellow"]
		dictEnemies3 = ["yellow"]
		dictEnemies4 = ["purple"]
		dictEnemies5 = ["orange"]
		dictEnemies6 = ["green"]
		dictEnemies = [dictEnemies0, dictEnemies1, dictEnemies2, dictEnemies3, dictEnemies4, dictEnemies5, dictEnemies6]
		latestTeam = None
		noEnemies = -1
		latestPlayer = None
		noPlayers = -1
		color = "white"
		isAlly = False

		for pawn in self.pawns:
			if pawn.player.team == team:
				isAlly = True
				if pawn.player.team != latestTeam:
					latestTeam = pawn.player.team
					noPlayers = -1
				if pawn.player != latestPlayer:
					noPlayers += 1
					latestPlayer = pawn.player
				if pawn == thisPawn:
					return dictAllies[noPlayers]
			else:
				isAlly = False
				if pawn.player.team != latestTeam:
					noEnemies += 1
					latestTeam = pawn.player.team
					noPlayers = -1
				if pawn.player != latestPlayer:
					noPlayers += 1
					latestPlayer = pawn.player
				if pawn == thisPawn:
					faction = dictEnemies[noEnemies]
					return faction[noPlayers]

	def drawInfocard(self, team = None, pawn = None, player = None):
		image = Image.open("{}/sprites/infocard.png".format(folder))

		#Draw pawn
		if pawn == None:
			pawn = self.pawns[self.current_turn]
		color = self.determineColor(pawn, team)
		pawn.update_sprite(pawn.player.team == team, True)
		if pawn.player.team == team:
			pawn.draw_on(image, 86, 4, pawn.colored_images[color])
		else:
			pawn.draw_on(image, 85, 4, pawn.colored_images[color])

		#Draw name & alliance
		if player != None:
			if pawn.player.Id == player.Id:
				teamShield = Image.open("{}/sprites/shield_you.png".format(folder))
				teamShield = content.drawText(teamShield, str(pawn.player.Id), "smallWide", 3, 2, (0, 91, 127))
			elif pawn.player.team == team:
				teamShield = Image.open("{}/sprites/shield_ally.png".format(folder))
				teamShield = content.drawText(teamShield, str(pawn.player.Id), "smallWide", 3, 2, (0, 102, 34))
			elif pawn.player.team != team:
				teamShield = Image.open("{}/sprites/shield_enemy.png".format(folder))
				teamShield = content.drawText(teamShield, str(pawn.player.Id), "smallWide", 3, 2, (127, 14, 22))	
			image.paste(teamShield, (103, 3), teamShield)
		else:
			image = content.drawText(image, str(pawn.player.Id), "smallWide", 106, 5, (95, 95, 95))		
		image = content.drawText(image, pawn.name, "large", 116, 3)

		#Draw HP hearts
		redHpLeftToDraw = pawn.Hp
		hpLeftToDraw = pawn.maxHp
		hpDrawPencilX = 0
		fullHeart = Image.open("{}/sprites/heart_bigfull.png".format(folder))
		halfHeart = Image.open("{}/sprites/heart_bighalf.png".format(folder))
		while hpLeftToDraw > 0:
			if redHpLeftToDraw >= 2:
				tag = "2"
				redHpLeftToDraw -= 2
			elif redHpLeftToDraw >= 1:
				tag = "1"
				redHpLeftToDraw -= 1
			elif redHpLeftToDraw >= 0:
				tag = "0"
			if redHpLeftToDraw < 1 and redHpLeftToDraw % 1 >= 0.5:
				tag += "h"
				redHpLeftToDraw -= 0.5
			if hpLeftToDraw >= 2:
				tag = "full" + tag
				hpLeftToDraw -= 2
				nextHeart = Image.open("{}/sprites/heart_{}.png".format(folder, tag))
				image.paste(nextHeart, (124 + hpDrawPencilX, 61), nextHeart)
				hpDrawPencilX += 6
			elif hpLeftToDraw == 1:
				tag = "half" + tag
				hpLeftToDraw -= 1
				nextHeart = Image.open("{}/sprites/heart_{}.png".format(folder, tag))
				image.paste(nextHeart, (124 + hpDrawPencilX, 61), nextHeart)
				hpDrawPencilX += 4

		#Draw lives counter
		noLives = min([pawn.respawns, pawn.player.respawns / len(pawn.player.pawns), pawn.player.team.respawns / pawn.player.team.noPawns()])
		whole = math.floor(noLives)
		partial = noLives - whole
		index = 0
		pasting = Image.open("{}/sprites/extralife.png".format(folder))
		while whole > 0:
			image.paste(pasting, (227 + 9 * index, 5), pasting)
			whole -= 1
			index += 1
		if partial > 0:
			pastingEmpty = Image.open("{}/sprites/extralife_empty.png".format(folder))
			image.paste(pastingEmpty, (227 + 9 * index, 5), pastingEmpty)
			pastingBands = pasting.split()
			pastingBands[3].paste(pastingBands[3].point(lambda i: i * partial)) #Multiply pixels in band 3 (alpha) by 75%
			pasting = Image.merge("RGBA", pastingBands)
			image.alpha_composite(pasting, (227 + 9 * index, 5))
			partial -= 1
			index += 1
		pasting = Image.open("{}/sprites/extralife_skull.png".format(folder))
		yeetDeaths = pawn.deathCounter
		while yeetDeaths > 0:
			image.paste(pasting, (227 + 9 * index, 5), pasting)
			yeetDeaths -= 1
			index += 1

		#Draw HP & MP bars
		#emptybar = Image.open("{}/sprites/barlong_empty.png".format(folder))
		#hpbar = Image.open("{}/sprites/barlong_hp.png".format(folder))
		#mpbar = Image.open("{}/sprites/barlong_mp.png".format(folder))

		#pixelsHp = min(math.ceil(pawn.Hp / 3),70)
		#pixelsMaxHp = min(math.ceil(pawn.maxHp / 3),70)
		#hpbar = hpbar.crop((0,0, pixelsHp,4))
		#maxhpbar = emptybar.crop((0,0, pixelsMaxHp,4))

		#pixelsMp = min(math.ceil(pawn.Mp / 3),70)
		#pixelsMaxMp = min(math.ceil(pawn.maxMp / 3),70)
		#mpbar = mpbar.crop((0,0, pixelsMp,4))
		#maxmpbar = emptybar.crop((0,0, pixelsMaxMp,4))

		#image.alpha_composite(maxhpbar, (102 + (70-pixelsMaxHp)//2, 73))
		#image.alpha_composite(hpbar, (102 + (70-pixelsMaxHp)//2, 73))
		#image.alpha_composite(maxmpbar, (102 + (70-pixelsMaxMp)//2, 91))
		#image.alpha_composite(mpbar, (102 + (70-pixelsMaxMp)//2, 91))

		#Draw item stats
		for i in range(len(pawn.inventory)):
			item = pawn.inventory[i]
			dictX = [214, 214, 258, 258]
			dictY = [16, 43, 16, 43]
			if item != None:
				icon = item.imageGet(0)
				image.paste(icon, (dictX[i], dictY[i]), icon)

		image = image.resize((800, 196))
		return image

	async def broadcast_infocard(self, pawn = None, where = None, player = None):
		if where == None:
			where = self.teams + self.lobbies

		for channel in where:
			infocard = self.drawInfocard(channel, pawn, player)
			name = "infocard-{}.png".format(random.randrange(1000000))
			path = "{}/tmp/{}".format(folder, name)
			infocard.save(path)
			with open(path, "rb") as f:
				if channel in self.teams and self.pawns[self.current_turn].player in channel.channel.players:
					await channel.channel.channel.send(file = discord.File(f)) #Fufufu.
			asyncio.ensure_future(delete_after(60, path))

class Team:
	def __init__(self, channel):
		self.channel = channel
		self.players = channel.players

async def delete_after(time, filepath):
	await asyncio.sleep(time)
	os.remove(filepath)